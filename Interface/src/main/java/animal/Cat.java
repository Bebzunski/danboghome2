package animal;

public class Cat implements Animal{
	
	private String name;

	public Cat(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String makeNoise(){
		return "Mia� Mia�";
	}

}
