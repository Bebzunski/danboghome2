package animal;

public class AnimalMain {

	public static void main(String[] args) {
		
		Animal d1 = new Dog("Reksio");
		Animal c1 = new Cat("Mruczek");
		Animal l1 = new Lion("King");
		
		System.out.println(d1.makeNoise());
		System.out.println(l1.makeNoise());
		System.out.println(c1.makeNoise());
		
	}
}
