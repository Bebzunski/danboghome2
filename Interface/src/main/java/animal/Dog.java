package animal;

public class Dog implements Animal{
	
	private String name;

	public Dog(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String makeNoise(){
		return "Ha� Ha�!!";
	}

}
