package animal;

public class Lion implements Animal{
	
	private String name;

	public Lion(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String makeNoise(){
		return "Rrrraaaawww";
	}

}
