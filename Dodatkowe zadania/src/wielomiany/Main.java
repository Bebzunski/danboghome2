package wielomiany;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		String a = new String("asd");
		String b = a.substring(1);
		a = "qqqqqq";

		System.out.println(a);
		System.out.println(b);

		System.exit(0);

		List<Integer> lista1 = new ArrayList<>();
		List<Integer> lista2 = lista1;
		lista1.add(5);
		lista2.add(7);

		System.out.println(lista1.get(1));
		System.out.println(lista2.get(1));

		int[] wielomian = { 3, -1, 5, 2 };

		File f = new File("resources/wyniki.txt");

		try {
			FileWriter fw = new FileWriter(f);
			fw.write("Stopie� wielomianu = " + wielomian.length + "\n");
			fw.write("Wsp�czynniki wielonianu : ");
			for (int wsp : wielomian) {
				fw.write(wsp + " ");
			}
			fw.write("\n");
			for (double arg = -2.0; arg <= 3; arg += 0.125) {
				fw.write("(" + arg + " , " + round(getValue(wielomian, arg), 2) + ")\n");
			}
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static double getValue(int[] wielomian, double argument) {
		double value = 0;
		for (int i = 0; i < wielomian.length; i++) {
			value += wielomian[i] * potega(argument, i);
		}
		return value;
	}

	public static double potega(double argument, int n) {
		double value = 1;
		for (int i = 0; i < n; i++) {
			value = value * argument;
		}
		return value;
	}

	public static double round(double num, int place) {
		int temp = (int) (num * potega(10, place));
		double round = ((double) temp) / potega(10, place);
		return round;
	}

}
