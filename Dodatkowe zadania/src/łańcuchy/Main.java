package �a�cuchy;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		countLastChar(sc);
		sc.close();
	}

	public static void countLastChar(Scanner sc) {
		
		String line = sc.nextLine();
		char lastChar = line.charAt(line.length() - 1);
		int count = 1;
		for (int i = 0; i < line.length() - 2; i++) {
			if (line.charAt(i) == lastChar) {
				count++;
			}
		}
		System.out.println(count);
	}

}
