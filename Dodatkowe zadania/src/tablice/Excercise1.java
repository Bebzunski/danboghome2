package tablice;

import java.util.Random;

public class Excercise1 {
	
	public static void doAllMethods() {
		
		int[] randomInts = getRandomIntegers(10, -10, 10);
		System.out.println("Wylosowane liczby:");
		printArray(randomInts);
		System.out.println("\nMin = "+minInArray(randomInts)+", Max = "+maxInArray(randomInts));
		System.out.println("\n�rednia arytmetyczna = "+avg(randomInts));
		System.out.println("\nIle mniejszych od �redniej = "+smallerThanAvg(randomInts));
		System.out.println("\nIle wiekszych od �redniej = "+biggerThanAvg(randomInts)+"\n");
		printArrayFromRightToLeft(randomInts);
	}

	public static int[] getRandomIntegers(int number, int from, int to) {
		int[] randomIntegers = new int[number];
		Random r = new Random();
		for (int i = 0; i < number; i++) {
			randomIntegers[i] = r.nextInt(to-from+1) + from;
		}
		return randomIntegers;
	}

	public static int maxInArray(int... array) {
		int currentMax = array[0];
		for (int i = 1; i < array.length; i++) {
			currentMax = max(currentMax, array[i]);
		}
		return currentMax;
	}

	public static int minInArray(int... array) {
		int currentMin = array[0];
		for (int i = 1; i < array.length; i++) {
			currentMin = min(currentMin, array[i]);
		}
		return currentMin;
	}

	public static int max(int a, int b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}

	public static int min(int a, int b) {
		if (a > b) {
			return b;
		} else {
			return a;
		}
	}

	public static double avg(int[] arr) {
		double sum = 0;
		for (int num : arr) {
			sum += num;
		}
		return sum / arr.length;
	}

	public static int smallerThanAvg(int[] arr) {
		int count = 0;
		double avg = avg(arr);
		for (int num : arr) {
			if (num < avg) {
				count++;
			}
		}
		return count;
	}

	public static int biggerThanAvg(int[] arr) {
		int count = 0;
		double avg = avg(arr);
		for (int num : arr) {
			if (num > avg) {
				count++;
			}
		}
		return count;
	}

	public static void printArray(int[] array) {
		for (int num : array) {
			System.out.print(num + " ");
		}
		System.out.println();
	}

	public static void printArrayFromRightToLeft(int[] array) {
		for (int i = array.length - 1; i >= 0; i--) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

}
