package tablice;

public class Excerise2 {

	public static void doAll(int number, int min, int max) {
		int[] randomInts = Excercise1.getRandomIntegers(number, min, max);
		Excercise1.printArray(randomInts);
		int[] count = countNumbers(randomInts, min, max);
		print(count, min, max);
	}

	public static int[] countNumbers(int[] array, int min, int max) {
		int[] count = new int[max - min + 1];
		for (int num : array) {
			count[num-min]++;
		}
		return count;
	}

	public static void print(int[] count, int min, int max) {
		for (int i = 0; i < count.length; i++) {
			System.out.println((min+i) + " - " + count[i] + " razy");
		}
	}

}
