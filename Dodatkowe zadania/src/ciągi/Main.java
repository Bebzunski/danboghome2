package ci�gi;

import java.security.MessageDigest;

public class Main {

	public static void main(String[] args) {

		System.out.println(ifPalindrom(12345654321l));
		System.out.println(sha256("Jacek"));
		System.out.println(sha256("Daniel"));
	}

	public static int sum(int n) {
		int sum = 0;
		int a = 1;
		int b = 2;
		int c = 7;
		for (int i = 1; i <= n; i++) {
			if (i % 3 == 1) {
				sum += a;
				a += 10;
			} else if (i % 3 == 2) {
				sum += b;
				b += 10;
			} else {
				sum += c;
				c += 10;
			}
		}
		return sum;
	}

	public static boolean ifSumIsTen(long number) {
		int sum = 0;
		String parseNumber = "" + number;
		for (int i = 0; i < parseNumber.length(); i++) {
			sum += Integer.parseInt(parseNumber.substring(i, i + 1));
		}
		return sum == 10;
	}

	public static long sumContainTen(int n) {
		long sum = 0;
		int i = 0;
		long number = 19;
		while (i < n) {
			if (ifSumIsTen(number)) {
				sum += number;
				i++;
			}
			number++;
		}
		return sum;
	}

	public static int NWW(int smaller, int bigger) {
		int a = smaller;
		int b = bigger;
		while (a != b) {
			while (a < b) {
				a += smaller;
			}
			while (a > b) {
				b += bigger;
			}
		}
		return a;
	}

	public static boolean ifPalindrom(long number) {
		String parseNumber = "" + number;
		int length = (parseNumber.length() + 1) / 2;
		for (int i = 0; i < length; i++) {
			if (parseNumber.charAt(i) != parseNumber.charAt(parseNumber.length() - 1 - i)) {
				return false;
			}
		}
		return true;
	}
	
	
	public static String sha256(String base) {
	    try{
	        MessageDigest digest = MessageDigest.getInstance("SHA-256");
	        byte[] hash = digest.digest(base.getBytes("UTF-8"));
	        StringBuffer hexString = new StringBuffer();

	        for (int i = 0; i < hash.length; i++) {
	            String hex = Integer.toHexString(0xff & hash[i]);
	            if(hex.length() == 1) hexString.append('0');
	            hexString.append(hex);
	        }

	        return hexString.toString();
	    } catch(Exception ex){
	       throw new RuntimeException(ex);
	    }
	}
}
