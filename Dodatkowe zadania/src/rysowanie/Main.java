package rysowanie;

public class Main {

	public static void main(String[] args) {

		drawBigLetterA(4);

	}

	public static String drawEmptyLineOfA(int length) {
		String line = "A";
		for (int i = 2; i < 2 * length - 1; i++) {
			line += " ";
		}
		if (length > 1) {
			line += "A";
		}
		return line;
	}

	public static String drawFullLineOfA(int length) {
		String line = "A";
		for (int i = 1; i < 2 * length - 1; i++) {
			line += "A";
		}
		return line;
	}

	public static void centerLine(String line, int length) {
		int diffrence = 2 * length - 1 - line.length();
		String space = "";
		if (diffrence >= 0) {
			for (int i = 0; i < diffrence / 2; i++) {
				space += " ";
			}
			System.out.println(space + line + space);
		}
	}

	public static void drawBigLetterA(int size) {
		for (int i = 1; i <= 2 * size - 1; i++) {
			if (i != size) {
				centerLine(drawEmptyLineOfA(i), 2 * size - 1);
			} else {
				centerLine(drawFullLineOfA(i), 2 * size - 1);
			}
		}
	}
}
