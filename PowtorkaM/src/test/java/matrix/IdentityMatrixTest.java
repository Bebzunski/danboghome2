package matrix;


import static org.junit.Assert.*;
import org.junit.Test;

import calc.Matrix;

public class IdentityMatrixTest {
	

	@Test
	public void whenEmptyArrayGivenIdentityMatrixExpected() {
		int[][] actual = new int[3][3];
		int[][] expected = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };		
		assertArrayEquals(expected, Matrix.identityMatrix(actual));
	}
	
	@Test 
	public void whenEmptyArrayGivenIdentityMatrixExpectedByValue() {
		int[][] actual = new int[3][3];
		int[][] expected = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		actual = Matrix.identityMatrix(actual);
		
		for(int i = 0; i < actual.length; i++) {
			for(int j = 0; j < actual[i].length; j++) {
				assertEquals(expected[i][j], actual[i][j]);
			}
		}
	}
	
	@Test
	public void whenEmptyArrayGivenCheckDimensionsOfArray() {
		int[][] actual = new int[3][3];
		int[][] expected = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		actual = Matrix.identityMatrix(actual);
		
		assertTrue("First dimension value fail", actual.length == expected.length);
		
		for(int i = 0; i < actual.length; i++) {
			assertTrue("Values are not equal", actual[i].length == expected[i].length);
		}
	}
	
	@Test(expected=Exception.class)
	public void whenNotSquareArrayGivenExceptionExpected(){
		int[][] actual = new int[3][2];
		Matrix.identityMatrix(actual);
	}
	
	@Test(expected=Exception.class)
	public void whenNotMatrixArrayGivenExceptionExpected(){
		int[][] actual = {{2,1},{3,4,2},{1}};
		Matrix.identityMatrix(actual);
	}

}
