package komputer;

public abstract class BasicComputer {
	
	
	public abstract void externalDevice();
	
	public void devices() {
        motherboard();
        processor();
        externalDevice();
        System.out.println();
    }

    public void motherboard() {
        System.out.println("Motherboard");
    }

    public void processor() {
        System.out.println("Processor");
    }
    
}
