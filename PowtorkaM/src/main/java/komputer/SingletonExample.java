package komputer;

public class SingletonExample {

	private static SingletonExample instance = null;
	private String name;

	private SingletonExample() {

	}

	public static SingletonExample getInstance() {
		if (instance == null) {
			System.out.println("Tworz� instancje.");
			instance = new SingletonExample();
		}
		System.out.println("Zwracam instancje, bo juz jest utworzona.");
		return instance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
