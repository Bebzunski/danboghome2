package komputer;

import java.util.LinkedList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		List<Person> people = new LinkedList<>();

		people.add(new Person("Pawel", "Testowy", 30));
		people.add(new Person("Pawel", "Inne", 42));
		people.add(new Person("Mirek", "Przykladowy", 22));
		people.add(new Person("Darek", "Testowy", 44));

		People pp = new People().addGroup("new", people);
		for(Person person : pp.from("new").name("Pawel").lastname("Inne").get()){
			System.out.println(person);
		}

		System.exit(0);

		SingletonExample se1 = SingletonExample.getInstance();
		se1.setName("Daniel");
		System.out.println(se1.getName());

		SingletonExample se2 = SingletonExample.getInstance();
		se2.setName("JAcek");

		SingletonExample se3 = SingletonExample.getInstance();
		System.out.println(se3.getName());

	}

}
