package typyGeneryczne;

import java.util.ArrayList;
import java.util.List;

public class ListTool {
	
	public static void main(String[] args) {
		List<Integer> list = toList(3,2,7,6,45);
		StringBuilder table = new StringBuilder();
		for(Integer num : list){
			table.append(num+" ");
		}
		System.out.println(table.toString().trim());
		
		
	}
	
	@SafeVarargs
	public static <T> List<T> toList(T... numbers){
		List<T> list = new ArrayList<T>();
		for(T num : numbers){
			list.add(num);
		}
		return list;
	}

}
