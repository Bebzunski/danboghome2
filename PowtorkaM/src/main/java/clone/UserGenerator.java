package clone;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {

	private final String path = "src/main/resources/";
	
	public User getRandomUser(){
		UserSex sex = UserSex.SEX_MALE;
		if(new Random().nextInt(2) == 0){
			sex = UserSex.SEX_FEMALE;
		}
		return getRandomUser(sex);
	}

	public User getRandomUser(UserSex sex) {
		
		User user = new User();
		user.setSex(sex);
		String name;
		if (sex.equals(UserSex.SEX_MALE))
			name = getRandomLineFromFile("name_m.txt").trim();
		else
			name = getRandomLineFromFile("name_f.txt").trim();
		user.setName(name);
		
		user.setSecondName(getRandomLineFromFile("lastname.txt").trim());
		user.setAddress(getRandomAddress());
		String dateString = getRandomBirthDate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		try {
			user.setBirthDate(sdf.parse(dateString));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return user;
	}

	public Address getRandomAddress() {
		Address result = new Address();
		String[] cityAndZip = getRandomLineFromFile("city.txt").split("\t");
		result.setCountry("Poland");
		result.setCity(cityAndZip[0].trim());
		result.setZipcode(cityAndZip[1].trim());
		result.setStreet("ul. " + getRandomLineFromFile("street.txt").trim());
		int number = new Random().nextInt(100) + 1;
		result.setNumber("" + number);
		return result;
	}

	public String getRandomLineFromFile(String filename) {
		File f = new File(path + filename);
		String currentLine = "";
		try (Scanner sc = new Scanner(f)) {
			Random r = new Random();
			int randomLine = r.nextInt(countLines(filename)) + 1;
			for (int i = 1; i < randomLine; i++) {
				sc.nextLine();
			}
			currentLine = sc.nextLine();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return currentLine;
	}

	public int countLines(String filename) {
		File f = new File(path + filename);
		int lines = 0;
		try (Scanner sc = new Scanner(f)) {

			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return lines;
	}

	public String getRandomBirthDate() {
		int year = 1890 + new Random().nextInt(120);
		int month = new Random().nextInt(12) + 1;
		int[] daysInMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if (year % 4 == 0) {
			daysInMonths[1]++;
		}
		int day = new Random().nextInt(daysInMonths[month - 1]) + 1;
		return ifZero(day) + "." + ifZero(month) + "." + year;
	}

	public String ifZero(int number) {
		if (number < 10) {
			return "0" + number;
		} else
			return "" + number;
	}

}
