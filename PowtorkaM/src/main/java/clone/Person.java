package clone;

public class Person {
	

    private String name;

    public Person(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Person(name);
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Person jan = new Person("Jan");
        Person adam = (Person) jan.clone();
        adam.setName("Adam");
        System.out.println(jan.getName());
        System.out.println(adam.getName());
    }

}
