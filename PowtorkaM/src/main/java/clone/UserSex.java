package clone;

public enum UserSex {
	
	SEX_MALE(1, "M�czyzna"), SEX_FEMALE(2, "Kobieta"), SEX_UNDEFINED(3, "Nieznana");
	
	private int id;
	private String name;
	private UserSex(int id, String name) {
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

}
