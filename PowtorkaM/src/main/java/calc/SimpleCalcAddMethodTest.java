package calc;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class SimpleCalcAddMethodTest {

	private SimpleCalc sc;

	@Before
	public void doStuff() {
		sc = new SimpleCalc();
	}

	@Test
	public void whenTwoPositiveNumbersAreGivenPositiveNumberAreSum() {
		double a = 3.42, b = 25.58;
		assertEquals(29.0, sc.add(a, b), 0.01);
	}

	@Test(expected = Exception.class)
	public void whenExThrowMethodIsUsedExceptionIsThrown() throws Exception {
		sc.exThrow();
	}

	@Test
	public void whenTwoMaxIntegersAreGivenPositiveNumberAreExpected() {
		assertTrue("Out of range", sc.add(Integer.MAX_VALUE, Integer.MAX_VALUE) < 0);
	}
}
