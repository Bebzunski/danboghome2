package calc;

public class Matrix {

	public static void main(String[] args) {

		// int[][] test = { { 2, 5, 8, 5 }, { 1, 0, 6, -2 }, { 0, 4, 8, 34 } };
		int[][] test2 = { { 2, -3, 8 }, { -3, 4, 6 }, { 8, 6, 87 } };
//		int[][] test3 = { { 2, 3 }, { -4, 5 } };
		print2DArray(test2);
		print2DArray(deleteVerseAndColumn(test2, 0, 1));

	}

	public static int indicator3x3(int[][] matrix) {
		if (!isSquare(matrix) || matrix.length != 3) {
			throw new IllegalArgumentException("To nie jest macierz kwadratowa 3x3");
		}
		int indicator3 = 0;
		int indicator2;
		int sign = 1;
		for (int i = 0; i < matrix[0].length; i++) {
			indicator2 = indicator2x2(deleteVerseAndColumn(matrix, 0, i));
			indicator3 += sign * matrix[0][i] * indicator2;
			sign *= -1;
		}
		return indicator3;
	}

	public static int indicator2x2(int[][] matrix) {
		if (!isSquare(matrix) || matrix.length != 2) {
			throw new IllegalArgumentException("To nie jest macierz kwadratowa 2x2");
		}
		return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
	}

	public static int[][] multiplicateTwoMatrix(int[][] left, int[][] right) {
		if (!isMatrix(left) || !isMatrix(right)) {
			throw new IllegalArgumentException("To nie s� macierze");
		}
		if (left[0].length != right.length) {
			throw new IllegalArgumentException("Nie mo�na wymno�y� macierzy, r�ne d�ugo�ci");
		}
		int[][] multi = new int[left.length][right[0].length];
		int[][] rightTrans = transpose(right);
		for (int i = 0; i < multi.length; i++) {
			for (int j = 0; j < multi[0].length; j++) {
				multi[i][j] = linearCombination(left[i], rightTrans[j]);
			}
		}
		return multi;
	}

	public static boolean isSymetric(int[][] m) {
		if (!isSquare(m)) {
			return false;
		}
		int[][] trans = transpose(m);
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m.length; j++) {
				if (m[i][j] != trans[i][j]) {
					return false;
				}
			}
		}
		return true;
	}

	public static int[][] transpose(int[][] m) {
		if (!isMatrix(m)) {
			throw new IllegalArgumentException("To nie jest macierz, nie mo�na jej transponowa�");
		}
		int[][] trans = new int[m[0].length][m.length];
		for (int i = 0; i < trans.length; i++) {
			for (int j = 0; j < trans[0].length; j++) {
				trans[i][j] = m[j][i];
			}
		}
		return trans;
	}

	public static int[][] multiplicateMatrix(int[][] m, int n) {
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				m[i][j] *= n;
			}
		}
		return m;
	}

	public static int[][] addMatrix(int[][] a, int[][] b) {
		if (!isEqualDimension(a, b)) {
			throw new IllegalArgumentException("Tablice nie s� jednakowej d�ugo�ci!");
		}

		int[][] sum = a;
		for (int i = 0; i < sum.length; i++) {
			for (int j = 0; j < sum[i].length; j++) {
				sum[i][j] += b[i][j];
			}
		}
		return sum;
	}

	public static int[][] substractMatrix(int[][] a, int[][] b) {
		if (!isEqualDimension(a, b)) {
			throw new IllegalArgumentException("Tablice nie s� jednakowej d�ugo�ci!");
		}

		int[][] sum = a;
		for (int i = 0; i < sum.length; i++) {
			for (int j = 0; j < sum[i].length; j++) {
				sum[i][j] = sum[i][j] - b[i][j];
			}
		}
		return sum;
	}

	public static boolean isEqualDimension(int[][] a, int[][] b) {
		if (a.length != b.length) {
			return false;
		}
		for (int i = 0; i < a.length; i++) {
			if (a[i].length != b[i].length) {
				return false;
			}
		}
		return true;
	}

	public static int[][] indexedMatrix(int[][] array) {
		int index = 1;
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = index;
				index++;
			}
		}
		return array;
	}

	public static void print2DArray(int[][] array) {

		for (int[] vector : array) {
			for (int num : vector) {
				System.out.printf("%5d", num);
			}
			System.out.println();
		}
		System.out.println();
	}

	public static int[][] identityMatrix(int[][] matrix) {

		if (!isSquare(matrix)) {
			throw new IllegalArgumentException("To nie jest macierz kwadratowa");
		}
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				matrix[i][j] = 0;
				if (i == j)
					matrix[i][j] = 1;
			}
		}
		return matrix;
	}

	public static int linearCombination(int[] a, int[] b) {
		if (a.length != b.length) {
			throw new IllegalArgumentException("Wektory nie s� r�wnej d�ugo�ci");
		}

		int result = 0;
		for (int i = 0; i < a.length; i++) {
			result += a[i] * b[i];
		}
		return result;
	}

	public static boolean isSquare(int[][] array) {

		if (!isMatrix(array)) {
			return false;
		}
		return array.length == array[0].length;
	}

	public static boolean isMatrix(int[][] array) {
		for (int i = 1; i < array.length; i++) {
			if (array[i].length != array[0].length) {
				return false;
			}
		}
		return true;
	}

	public static int[][] deleteVerseAndColumn(int[][] matrix, int verse, int column) {
		return deleteColumn(deleteVerse(matrix, verse), column);

	}

	public static int[][] deleteVerse(int[][] matrix, int verse) {
		if (!isMatrix(matrix)) {
			throw new IllegalArgumentException("To nie jest macierz");
		}

		if (verse < 0 || verse >= matrix.length) {
			throw new IllegalArgumentException("Nie ma takiego wiersza");
		}

		int[][] result = new int[matrix.length - 1][matrix[0].length];

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (i < verse)
					result[i][j] = matrix[i][j];
				if (i > verse)
					result[i - 1][j] = matrix[i][j];
			}
		}
		return result;

	}

	public static int[][] deleteColumn(int[][] matrix, int column) {
		if (!isMatrix(matrix)) {
			throw new IllegalArgumentException("To nie jest macierz");
		}

		if (column < 0 || column >= matrix[0].length) {
			throw new IllegalArgumentException("Nie ma takiej kolumny");
		}

		int[][] result = new int[matrix.length][matrix[0].length - 1];

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (j < column)
					result[i][j] = matrix[i][j];
				if (j > column)
					result[i][j - 1] = matrix[i][j];
			}
		}
		return result;

	}

}
