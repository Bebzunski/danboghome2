package calc;

public class SimpleCalc {
	
	public int add(int a, int b) {
		return a + b;
	}
	
	
	public double add(double a, double b) {
		return a + b;
	}
	
	
	public int exThrow() throws Exception {
		throw new Exception("Wyjatek");
	}

}
