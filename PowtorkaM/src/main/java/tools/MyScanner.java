package tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MyScanner {
	
	private BufferedReader reader;
	
	public MyScanner(InputStream is){
		reader = new BufferedReader(new InputStreamReader(is));
	}
	
	public MyScanner(){
		this(System.in);
	}
	
	public int nextInt(){
		try {
			return Integer.parseInt(reader.readLine());
		} catch (NumberFormatException | IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public int[] nextArray(){
		try {
			String[] table = reader.readLine().split(",");
			int[] results = new int[table.length];
			for(int i = 0 ; i < table.length ; i++){
				results[i] = Integer.parseInt(table[i]);
			}
			return results;
		} catch (NumberFormatException |IOException e) {
			throw new RuntimeException(e);
		}
		
	}

	public BufferedReader getReader() {
		return reader;
	}

	public void setReader(BufferedReader reader) {
		this.reader = reader;
	}
	
	
}
