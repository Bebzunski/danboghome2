package watki;

@FunctionalInterface
public interface TransformInterface {
	
	public String transform(String s);

}
