package watki;

public class Calc {

	public double oper(int a, int b, CalculatorInterface ci) {
		return ci.doOperation(a, b);
	}

}
