package watki;

import java.util.LinkedList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		List<Employee> listOfEmps = new LinkedList<>();
		
		listOfEmps.add(new Employee("Pawe�", "Testowy", 18, 3000));
		listOfEmps.add(new Employee("Jola", "Przykladowy", 19, 5000));
		listOfEmps.add(new Employee("Piotr", "Przykladowy", 19, 5000));
		
		Company company = new Company();
		company.setEmployees(listOfEmps);
		company.filter(s -> (s.startsWith("J")), s -> (s.toUpperCase()));
		
		String test = "test";
		System.out.println(test.startsWith("tea"));
		
		Animals a = new Animals();
		a.addAnimal("pies.kot.ptak", (animals) -> (animals.split("\\.")));
		for(int i = 0 ; i < a.getAnimals().size() ; i++){
			System.out.println(a.getAnimal(i));
		}
		
		a.addAnimal("katp", (anims) -> {
			String[] splited = anims.split("");
			StringBuilder animal = new StringBuilder();
			for(int i = splited.length -1 ; i >= 0 ; i--){
				animal.append(splited[i]);
			}
			return new String[]{animal.toString()};
			
		});
		a.getAnimal(a.getAnimals().size()-1);
		System.exit(0);

		Thread t = new Thread(new PrzykladowyWatek());
		t.start();

		new Thread(new InnyWatek(), "jaki� w�tek").start();
		System.out.println("KONIEC");

		new Thread(() -> {
			int f = 1, b = 3;
			System.out.println("Suma a+b = " + (f + b));
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println("Pochodze z kodu Runnable");
			}
		}).start();

		new Thread(() -> System.out.println("Wiadomosc z FI")).start();

		List<Person> p = new LinkedList<>();
		p.add(new Person(23));
		Calc c = new Calc();

		System.out.println(c.oper(5, 7, new CalculatorInterface() {

			@Override
			public int doOperation(int a, int b) {
				return a + b + 1;
			}
		}));

		System.out.println(c.oper(2, 3, (d, b) -> (d * b)));
		
		System.out.println( c.oper(17,  3, (q, b) -> ( q * b ) ) );

	}

}
