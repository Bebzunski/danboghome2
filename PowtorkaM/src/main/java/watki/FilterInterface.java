package watki;

@FunctionalInterface
public interface FilterInterface {

	public boolean test(String s);
}
