package watki;

@FunctionalInterface
public interface CalculatorInterface {
	
	int doOperation(int a, int b);

}
