package inputOutput;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BinaryFile implements IFile {

	private final static String FILE_NAME = "binary.txt";

	public void save(List<Student> studentsList) {

		DataOutputStream out = null;
		try {
			FileOutputStream fos = new FileOutputStream(PATH + FILE_NAME);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			out = new DataOutputStream(bos);
			for (Student student : studentsList) {
				out.writeUTF(student.getSurname());
				out.writeInt(student.getIndexNumber());
				out.writeUTF(student.getName());
				
			}
			out.close();
		} catch (FileNotFoundException e) {
			e.getStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<Student> load() {
		List<Student> students = new ArrayList<Student>();
		try {
			DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(PATH + FILE_NAME)));
			while (in.available() > 0) {
				
				String name = in.readUTF();
				int indexNumber = in.readInt();
				String surname = in.readUTF();
				Student student = new Student(indexNumber, name, surname);
				students.add(student);
			}
			in.close();
		} catch (FileNotFoundException e) {
			// TODO: handle exception
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return students;
	}

}
