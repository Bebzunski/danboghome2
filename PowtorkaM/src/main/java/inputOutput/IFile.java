package inputOutput;

import java.util.List;

public interface IFile {
	
	static String PATH = "src/main/resources/";
	
	void save(List<Student> studentsList);

	List<Student> load();

}
