package inputOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class TextFile {

	private final static String FILE_NAME = "students_text.txt";

	public void save(List<Student> studentsList) {
		File f = new File("src/main/resources/" + FILE_NAME);
		try {
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			for (Student student : studentsList) {
				pw.println(student.toString());
			}
			pw.close();
		} catch (FileNotFoundException e) {
			// TODO: handle exception
		}

	}

	public List<Student> load() {
		List<Student> students = new LinkedList<Student>();
		File f = new File("src/main/resources/" + FILE_NAME);
		try {
			Scanner sc = new Scanner(f);
			String currentLine;
			String[] splited;
			int index;
			String name;
			String surname;
			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				splited = currentLine.split(", ");
				index = Integer.parseInt(splited[0].substring(21).trim());
				name = splited[1].substring(5).trim();
				surname = splited[2].substring(8, splited[2].length() - 1).trim();
				students.add(new Student(index, name, surname));
			}
			sc.close();
		} catch (FileNotFoundException e) {
			// TODO: handle exception
		}
		return students;
	}

}
