package inputOutput;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class University {
	
	private Map<Integer, Student> students = new HashMap<Integer, Student>();

	public University() {
	}

	public University(List<Student> studentsList) {

		for (Student student : studentsList) {
			students.put(student.getIndexNumber(), student);
		}
	}

	public void addStudent(int indexNumber, String name, String surname) {
		Student student = new Student(indexNumber, name, surname);
		students.put(student.getIndexNumber(), student);
	}

	public boolean studentExists(int indexNumber) {
		// Delegacja
		return students.containsKey(indexNumber);
	}

	public Student getStudent(int indexNumber) {
		// Delegacja
		return students.get(indexNumber);
	}

	public int studentsNumber() {
		return students.size();
	}

	public void showAll() {
		for (Student s : students.values()) {
			System.out.println(s.getIndexNumber() + " : " + s.getName() + " " + s.getSurname());
		}
	}

	public List<Student> getStudentsList() {

		List<Student> sList = new LinkedList<Student>();

		for (Student s : students.values()) {

			sList.add(s);

		}

		return sList;
	}

}
