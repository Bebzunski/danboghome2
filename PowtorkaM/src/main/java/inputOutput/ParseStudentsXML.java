package inputOutput;

import static inputOutput.ParseXML.*;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ParseStudentsXML {
	
	public static void printStudentsInfo(String filename) throws ParserConfigurationException, SAXException, IOException{
		Document doc = parseFile(filename);
		NodeList students = doc.getElementsByTagName("student");
		for(int i = 0 ; i < students.getLength() ; i++){
			Node student = students.item(i);
			printStudentInfo(student);
		}
		
	}
	
	private static void printStudentInfo(Node studentNode){
		Element student = (Element) studentNode;
		System.out.println("Student roll no : " + student.getAttribute("rollno"));
		System.out.println("First name : "+ getText(student, "firstname"));
		System.out.println("Lastname : "+ getText(student, "lastname"));
		System.out.println("Nickname : "+ getText(student, "nickname"));
		System.out.println("Marks : " + getText(student, "marks"));
		System.out.println();
	}

}
