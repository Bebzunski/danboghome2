package inputOutput;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class SerializedFile implements IFile {

	private final static String FILE_NAME = "serialized.txt";
	
	public void save(List<Student> studentsList) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(PATH + FILE_NAME));
//			for (Student student : studentsList) {
				oos.writeObject(studentsList);
//			}
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO: handle exception
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Student> load() {
		try{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(PATH + FILE_NAME));
			List<Student> students = (List<Student>) ois.readObject();
			ois.close();
			return students;
		} catch (FileNotFoundException e) {
			// TODO: handle exception
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
