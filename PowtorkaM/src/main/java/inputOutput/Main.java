package inputOutput;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class Main {

	public static void main(String[] args) {
		
		try {
			ParseStudentsXML.printStudentsInfo("class.xml");
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(0);
		
		List<Student> students = new LinkedList<Student>();
		students.add(new Student(123, "Daniel", "Bodzio"));
		students.add(new Student(5353, "Kasia", "Nowak"));
		students.add(new Student(755, "Jacek", "Kurski"));
		IFile sf = new JsonFile();
		sf.save(students);
		
		List<Student> studentsLoad = sf.load();
		for(Student student : studentsLoad){
			System.out.println(student.getIndexNumber()+", "+student.getName()+", "+student.getSurname());
		}

	}

}
