package dziedziczenie;

public class Player {

	public static void main(String[] args) {

		for (int i = 0; i < 10; i++) {
			Music music = Spotify.selectRandomMusic();

			music.play();
			play(music);
			System.out.println();
		}

	}

	public static void play(Music music) {
		System.out.println("Gram muzyk�");
		music.play();
	}

	public static void play(Rap rap) {
		System.out.println("Gram rap");
		rap.play();
	}

}
