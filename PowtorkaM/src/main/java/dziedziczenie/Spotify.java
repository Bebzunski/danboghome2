package dziedziczenie;

import java.util.Random;

public class Spotify {

	private static Random random = new Random();

	public static Music selectRandomMusic() {
		int num = random.nextInt(2);
		if (num == 0)
			return new Techno();
		else
			return new Rap();
	}

}
