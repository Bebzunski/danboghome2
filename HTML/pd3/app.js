function fillDive(template, obj) {
    if(!(obj instanceof Array)) {
        obj = [ obj ];
    }
    var ret = "";
    for(let i = 0; i < obj.length; i++) {
        var keys = Object.keys(obj[i]);
        var newTemp = "" + template;
        for(let key of keys) {
            newTemp = newTemp.replace('[% ' + key + ' %]', obj[i][key]);
        }
        ret += newTemp;
    }
    return ret;
}

var data = [
    {   id: "postContainer1",
        title: "Drugi post",
        body: "Przykładowy drugi post",
        date: "20.07.2017"
    },
    {   id: "postContainer2",
        title: "Trzeci post",
        body: "Przykładowy trzeci post",
        date: "21.07.2017"
    },
    {   id: "postContainer3",
        title: "Czwarty post",
        body: "Przykładowy czwarty post",
        date: "22.07.2017"
    }

];

var template = '<div id=[% id %]>'
    + '<h2>[% title %]</h2>'
    + '<p>[% body %]</p>'
    +'<p>[% date %]</p>'
    + '</div>';

var body = document.getElementById('container');

body.innerHTML = fillDive(template, data);