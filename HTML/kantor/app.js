var exchangeButton = document.getElementById('exchange');
exchangeButton.addEventListener('click', function(){
    
    var amount = parseFloat(document.getElementById('amount').value);
    var selIndex = document.getElementById('currency').selectedIndex;
    var opts = document.querySelectorAll('#currency > option');
    
    var ret = round(amount * opts[selIndex].dataset.rate);
    var resultLabel = document.getElementById('wynik');
    var waluta = ['$','EUR','FUN'];
    resultLabel.innerHTML = ret +" "+ waluta[selIndex];
    
})

var round = function(arg){
    var ret = arg*100;
    return Math.round(ret)/100;
}