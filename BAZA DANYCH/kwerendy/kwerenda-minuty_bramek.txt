SELECT  "    od 01 do 15" as Minuty, 
               '     '&Count(*) as Ile
FROM   (
              SELECT *
              FROM wydarzenia 
              WHERE minuta>0 AND minuta<16 AND idw LIKE 'g*'
              ORDER BY minuta
             )
             AS tabela1

UNION
SELECT  "    od 16 do 30",
              '     '&Count(*)
FROM ( 
              SELECT * 
              FROM wydarzenia
              WHERE minuta>15 and minuta<31 and idw like 'g*'
              ORDER by minuta
            ) as tabela1
UNION
SELECT  "    od 31 do 45",
              '     '&Count(*)
FROM ( 
             SELECT *
             FROM wydarzenia
             WHERE minuta>30 and minuta<46 and idw like 'g*'
             ORDER by minuta)
             as tabela1;
UNION
SELECT   "    od 46 do 60",
                '     '&Count(*)
FROM ( 
             SELECT *
             FROM wydarzenia
             where minuta>45 and minuta<61 and idw like 'g*'
             ORDER by minuta
             ) as tabela1;
UNION
SELECT  "    od 61 do 75",
              '     '&Count(*)
FROM ( 
            SELECT *
            FROM wydarzenia
            WHERE minuta>60 and minuta<76 and idw like 'g*'
            ORDER by minuta
            ) as tabela1;
UNION
SELECT   "    od 76 do 90",
               '     '&Count(*)
FROM (
             SELECT *
             FROM wydarzenia
             WHERE minuta>75 and minuta<91 and idw like 'g*'
             ORDER by minuta
            ) as tabela1;
UNION
SELECT "    od 90",
              '     '&Count(*)
FROM (
             SELECT *
             FROM wydarzenia
             WHERE minuta>90 and idw like 'g*'
             ORDER by minuta
            ) as tabela1;
UNION
SELECT  "    WSZYSTKIE",
              '    '&Count(*)
FROM wydarzenia
WHERE idw like 'g*';
UNION
SELECT  "    �rednia",
              '   '&round(avg(GZ)*2,2)
FROM (
             SELECT *
             FROM mecze 
             WHERE gz is not null
            );
UNION
SELECT  "    max w meczu",
               '     '&max(razem)
FROM (
             SELECT * ,
                        (GZ+GS) as razem
             FROM mecze
             WHERE gz is not null and gs is not null
            );