SELECT  '   '&reprezentacja AS Reprezentacjaa,
              iif(Ilee>9,'     '&Ilee,'      '&Ilee) AS Ile
FROM (
             SELECT ISO,
                          count(*) AS Ilee
             FROM (
                          SELECT *
                          FROM (
                                       SELECT *
                                       FROM wydarzenia
                                       WHERE idw LIKE 'z*'
                                     )
                                      AS tabela1
                          LEFT JOIN pi�karze ON tabela1.pi�karz=pi�karze.idp
                        )
                        AS tabela2
              group by iso
            )
             AS tabela3 LEFT JOIN reprezentacje ON tabela3.iso=reprezentacje.iso
UNION
SELECT  '   WSZYSTKIE',
              '    '&count(wydarzenia.idw) AS Ile
FROM wydarzenia
WHERE idw like 'z*';