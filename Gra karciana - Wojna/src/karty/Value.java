package karty;

public enum Value {

	TWO(1, "2"), THREE(2, "3"), FOUR(3, "4"), FIVE(4, "5"), SIX(5, "6"), SEVEN(6 , "7"), EIGHT(7, "8"),
	NINE(8 , "9"), TEN(9, "T"), JACK(10 , "J"), QUEEN(11, "Q"), KING(12, "K"), ACE(13, "A");

	private int id;
	String figure;

	private Value(int id, String figure) {
		this.figure = figure;
		this.id = id;
	}

	private Value() {
	}

	public int getValue() {
		return id;
	}


	public String getFigure() {
		return figure;
	}

	public int getId() {
		return id;
	}
	
}
