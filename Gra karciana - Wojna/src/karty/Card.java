package karty;

public enum Card {
	
	HA(Suit.HEART, Value.ACE) , DA(Suit.DIAMOND, Value.ACE) , SA(Suit.SPADES, Value.ACE) , CA(Suit.CLUB, Value.ACE) ,
	HK(Suit.HEART, Value.KING) , DK(Suit.DIAMOND, Value.KING) , SK(Suit.SPADES, Value.KING) , CK(Suit.CLUB, Value.KING) ,
	HQ(Suit.HEART, Value.QUEEN) , DQ(Suit.DIAMOND, Value.QUEEN) , SQ(Suit.SPADES, Value.QUEEN) , CQ(Suit.CLUB, Value.QUEEN) ,
	HJ(Suit.HEART, Value.JACK) , DJ(Suit.DIAMOND, Value.JACK) , SJ(Suit.SPADES, Value.JACK) , CJ(Suit.CLUB, Value.JACK) ,
	HT(Suit.HEART, Value.TEN) , DT(Suit.DIAMOND, Value.TEN) , ST(Suit.SPADES, Value.TEN) , CT(Suit.CLUB, Value.TEN) ,
	H9(Suit.HEART, Value.NINE) , D9(Suit.DIAMOND, Value.NINE) , S9(Suit.SPADES, Value.NINE) , C9(Suit.CLUB, Value.NINE) ;
	
	private Value value;
	private Suit suit;
	
	private Card(Suit suit, Value value ) {
		this.value = value;
		this.suit = suit;
	}

	public Value getValue() {
		return value;
	}

	public Suit getSuit() {
		return suit;
	}
	
	public String printCard(){
		return "["+value.getFigure()+" "+suit.getColour()+"]";
	}
	
}
