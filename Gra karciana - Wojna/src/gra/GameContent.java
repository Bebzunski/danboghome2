package gra;

public class GameContent {

	private Game game;

	public GameContent(Game game) {
		this.game = game;
	}

	public void printIntro() {
		MyLine.placeCenter("GRA W WOJN�" + "\n");
		MyLine.placeCenter(game.getFirstPlayer().getPlayerName() + "   VS   " + game.getSecondPlayer().getPlayerName());
	}

	public void startingRoundContent(int nrOfWar) {
		System.out.println("------------------------------------------------------------------------");
		MyLine.placeCenter("RUNDA " + nrOfWar + ".");
	}
	
	public static String playersCardsContent(int n) {
		String cards = "";
		for (int i = 1; i <= n; i++) {
			cards += "[X]";
		}
		return cards;
	}
	
	public void warContent() {

		MyLine.placeLeft(game.getFirstPlayer().getPlayerName());
		MyLine.placeLeft(playersCardsContent(game.getFirstPlayer().getNumberOfCards()) + "\n");

		game.getPot().printPotContent();

		MyLine.placeRight(playersCardsContent(game.getSecondPlayer().getNumberOfCards()));
		MyLine.placeRight(game.getSecondPlayer().getPlayerName());
		System.out.println();

	}

	public void printCurrentResult() {

		MyLine.placeTwoCorners(
				game.getFirstPlayer().getPlayerName() + ": " + game.getFirstPlayer().getNumberOfCards() + " kart",
				game.getSecondPlayer().getPlayerName() + ": " + game.getSecondPlayer().getNumberOfCards() + " kart");
	}

	public void printEndingGame() {

		System.out.println();
		System.out.println();
		if (game.getSecondPlayer().getNumberOfCards() == 0) {
			MyLine.placeCenter(game.getFirstPlayer().getPlayerName() + " WYGRA� WOJN� !!");
		} else {
			MyLine.placeCenter(game.getSecondPlayer().getPlayerName() + " WYGRA� WOJN� !!");
		}
	}

}
