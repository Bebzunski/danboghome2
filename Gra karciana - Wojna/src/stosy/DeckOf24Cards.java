package stosy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import karty.Card;

public class DeckOf24Cards {
	
	public List<Card> deckOf24Cards = new ArrayList<>();

	public DeckOf24Cards() {
		
		deckOf24Cards.add(Card.C9); deckOf24Cards.add(Card.D9); deckOf24Cards.add(Card.S9); deckOf24Cards.add(Card.H9);
		deckOf24Cards.add(Card.CT); deckOf24Cards.add(Card.DT); deckOf24Cards.add(Card.ST); deckOf24Cards.add(Card.HT);
		deckOf24Cards.add(Card.CJ); deckOf24Cards.add(Card.DJ); deckOf24Cards.add(Card.SJ); deckOf24Cards.add(Card.HJ);
		deckOf24Cards.add(Card.CQ); deckOf24Cards.add(Card.DQ); deckOf24Cards.add(Card.SQ); deckOf24Cards.add(Card.HQ);
		deckOf24Cards.add(Card.CK); deckOf24Cards.add(Card.DK); deckOf24Cards.add(Card.SK); deckOf24Cards.add(Card.HK);
		deckOf24Cards.add(Card.CA); deckOf24Cards.add(Card.DA); deckOf24Cards.add(Card.SA); deckOf24Cards.add(Card.HA);
		
		shuffle();
		
	}
	
	public void shuffle(){
		
		Random r = new Random();
		List<Card> deckOfCards = new ArrayList<>();
		int size = deckOf24Cards.size();

		for(int j = 0 ; j < size ; j++){
			int randomIndex = r.nextInt(deckOf24Cards.size());
			deckOfCards.add(deckOf24Cards.remove(randomIndex));
		}
		deckOf24Cards = deckOfCards;
	}
	
	public void takeAwayCards(Player p1 , Player p2){
		for(int i = 0 ; i < 12 ; i++){
			p1.takeCard(deckOf24Cards.remove(0));
			p2.takeCard(deckOf24Cards.remove(0));
		}
	}
	
}
