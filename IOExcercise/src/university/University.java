package university;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class University {
	
	
	

	public static void main(String[] args) {
		
		University univ = new University();
		System.out.println(univ.isStudentExists(1));

	}
	
	private boolean isStudentExists(int index) {

		boolean ret = false;
		for (int i = 0; i < getListOfStudents().size(); i++) {
			if (getListOfStudents().get(i).getIndex() == index) {
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	
	private static ArrayList<Student> getListOfStudents() {

		ArrayList<Student> students = new ArrayList<>();
		File f = new File("resources/students.txt");
		try {

			Scanner sc = new Scanner(f);
			String currentLine;

			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				String[] currentStudent = currentLine.split("\t");
				students.add(new Student(Integer.parseInt(currentStudent[0]), currentStudent[1], currentStudent[2],
						currentStudent[3], Double.parseDouble(currentStudent[4])));
			}
			sc.close();

		} catch (FileNotFoundException e) {
			System.out.println("nie ma");
		}

		return students;
	}
}
