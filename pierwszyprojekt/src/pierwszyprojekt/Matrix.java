package pierwszyprojekt;

public class Matrix {
	
	public void printMatrix(int[][] arr){
		for(int[] rows : arr){
			for(int numbers : rows){
				System.out.print(numbers+" ");
			}
			System.out.println();
		}
	}
	
	public int[][] identityMatrix(int[][] arr) {

		for (int[] rows : arr) {
			for (int i = 0; i < rows.length; i++) {
				rows[i] = 1;
			}
		}
		return arr;
	}
	
	public int[][] indexedMatrix(int[][] arr) {
		int j = 1;
		for (int[] rows : arr) {
			for (int i = 0; i < rows.length; i++) {
				rows[i] = j++;
			}
		}
		return arr;
	}

	
	public boolean isEqualDimension(int[][] a, int[][] b) {
		
		if (a.length != b.length) {
			return false;
		} else {
			for (int i = 0; i < a.length; i++) {
				if (a[i].length != b[i].length) {
					return false;
				}
			}
		}
		return true;
	}
}
