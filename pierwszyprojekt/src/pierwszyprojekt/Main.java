package pierwszyprojekt;

public class Main {

	public static void main(String[] args) {

		int[][] array1 = {{6,5,1},{2,1,4},{2,7,4}};
		int[][] array2 = {{6,2,1},{2,1,0,4},{2,7,4}};
		
		Matrix m1 = new Matrix();
		m1.printMatrix(array1);
		System.out.println();
		m1.printMatrix(m1.identityMatrix(array1));
		
		System.out.println();
		m1.printMatrix(m1.indexedMatrix(array1));
		System.out.println();
		System.out.println(m1.isEqualDimension(array1,array2));

		
//		TwoDimArray t1 = new TwoDimArray(array1);
		
//		System.out.println(t1.find2DArrayMin());
	}

}
