package pierwszyprojekt;

public class TwoDimArray {
	
	private int[][] array;
	
	public TwoDimArray(int[][] array){
		this.array=array;
	}
	
	public int multiplyElements() {
		int product = 1;
		for (int[] rows : array) {
			for (int numbers : rows) {
				product *= numbers;
			}
		}
		return product;
	}

	public int multiplyEvenElements() {
		int product = 1;
		for (int[] rows : array) {
			for (int numbers : rows) {
				if (numbers % 2 == 0) {
					product *= numbers;
				}
			}
		}
		return product;
	}
	
	public void setArray(int[][] array){
		this.array = array;
	}
	
	public int[][] getArray(){
		return array;
	}
	

	public int find2DArrayMin() {
		int min = array[0][0];
		for (int[] rows : array) {
			for (int numbers : rows) {
				if (numbers < min) {
					min = numbers;
				}
			}
		}
		return min;
	}
}
