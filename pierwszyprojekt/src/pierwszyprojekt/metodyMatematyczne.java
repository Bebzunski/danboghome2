package pierwszyprojekt;

public class metodyMatematyczne {

	public static void main(String[] args) {
		sumowanieDwochLiczb(4,6);
		System.out.println(mnozenieDwochLiczb(3,5));
		
		System.out.println(porownywanieLiczb(2,9));
		
		System.out.println(najmniejszaLiczba(4,1,8));
		System.out.println(najmniejszaLiczba(4,4,7));
		System.out.println(najmniejszaLiczba(5,2,9));
		System.out.println(najmniejszaLiczba(5,3,1));
		
		int[] tablica = {25,8,11,90,13,55,6};
		
		System.out.println("Najmniejsza liczba w tablicy to : "+porownanieLiczb(tablica));
		System.out.println(czyRowne(3,3));
		
		// zrobic zadanie z wyszukanie liczby w tabeli i zwraca indeks, jak nie to zwraca -1
		
		System.out.println(szukajLiczby(tablica, 11));
		
	}

	private static int  szukajLiczby(int[] tablica, int szukana) {
		for (int i = 0; i < tablica.length; i++) {
			if (tablica[i] == szukana) {
				return i;
			}
		}
		return -1;

	}

	private static int porownanieLiczb(int[] tablica) {
		int porownanie = tablica[0];
		for(int i = 1 ; i<tablica.length;i++){
		porownanie = porownywanieLiczb(porownanie,tablica[i]);	
		}
		return porownanie;
	}

	private static int najmniejszaLiczba(int a, int b, int c) {
		int ab = porownywanieLiczb(a,b);
		int abc = 	porownywanieLiczb(ab,c);
		return abc;
	}
	
	private static void sumowanieDwochLiczb(int a, int b) {
		int suma = a+b;
		
		System.out.println(suma);
		}
	
	private static int mnozenieDwochLiczb(int a, int b) {
		int iloczyn = a*b;
		
		return iloczyn;
		}
	
	private static int porownywanieLiczb(int a, int b) {

		if(a<b) {
			return a;
		} else return b;

		}

	private static boolean czyRowne(int a, int b){
		if(a==b) return true;
		else return false;
	}
}
