package pierwszyprojekt;

import java.util.ArrayList;
import java.util.Random;

public class Zadanie {

	public static void main(String[] args) {

		sortingWithLists(9999, 100);

	}

	public static void sortingWithLists(int maxNumber, int howMany) {
		
		Random r = new Random();

		int[] randomNumbers = new int[howMany];

		for (int i = 0; i < howMany; i++) {
			randomNumbers[i] = r.nextInt(maxNumber);
		}

		long start = System.currentTimeMillis();

		ArrayList<Integer>[] arrayOfLists = new ArrayList[maxNumber];
		for (int i = 0; i < maxNumber; i++) {

			arrayOfLists[i] = new ArrayList<Integer>();
		}

		for (int i = 0; i < randomNumbers.length; i++) {

			arrayOfLists[randomNumbers[i]].add(randomNumbers[i]);
		}

		for (int i = 0; i < maxNumber; i++) {
			for (int j = 0; j < arrayOfLists[i].size(); j++) {
				System.out.println(arrayOfLists[i].get(0));
			}
		}

		long stop = System.currentTimeMillis();
		System.out.println(stop - start);
	}

	public static void sortingWithArrays(int maxNumber, int howMany) {

		Random r = new Random();
		int[] tablica = new int[howMany];
		for (int i = 0; i < howMany; i++) {
			tablica[i] = r.nextInt(maxNumber);
		}

		long start = System.currentTimeMillis();

		Integer[][] sortArray = new Integer[maxNumber][howMany];

		for (int i = 0; i < howMany; i++) {
			sortArray[tablica[i]][i] = tablica[i];
		}

		for (Integer[] arr : sortArray) {
			for (Integer num : arr) {
				if (num != null)
					System.out.println(num);
			}
		}

		long stop = System.currentTimeMillis();
		System.out.println(stop - start);

	}

}
