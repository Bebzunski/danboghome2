package dziedziczenie;

public class AnimalMain {

	public static void main(String[] args) {
		
		Animal mruczek = new Cat("czarny");
		Cat buszmen = new Cat("szary");

		showAnimal(mruczek);
		System.out.println();
		showAnimal(buszmen);
		System.out.println();

		
	}
	
	public static void showAnimal(Animal x){
		System.out.println("to jest animal "+x.getKolor());
		x.makeNoise();
	}

	public static void showAnimal(Cat x){
		System.out.println("to jest kot "+x.getKolor());
		x.makeNoise();
	}
	
}
