package dziedziczenie;

public class Animal {
	
	private String kolor;

	public Animal(String kolor) {
		super();
		this.kolor = kolor;
	}

	public String getKolor() {
		return kolor;
	}

	public void setKolor(String kolor) {
		this.kolor = kolor;
	}
	
	public void makeNoise(){
		System.out.println("nie mam g�osu");
	}

}
