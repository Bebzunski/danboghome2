package animals;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class Main {

	public static void main(String[] args) {

		Animal[] animals = { new Dog(), new Cat() };
		for (Animal anim : animals) {
			System.out.println(anim.makeNoise());
		}

			save5NoisesToFileTxt("tescik2", new Cat());
	}

	public static void save5NoisesToFileTxt(String filename, Animal animal){
		try (PrintStream ps = new PrintStream("src/" + filename + ".txt")) {
			for (int i = 0; i < 5; i++) {
				ps.println(animal.makeNoise());
			}

		} catch (FileNotFoundException e) {
			System.out.println("nie zapisa�o si�");
		}
	}

}
