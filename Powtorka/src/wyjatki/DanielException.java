package wyjatki;

public class DanielException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public DanielException(String text){
		super(text);
	}

}
