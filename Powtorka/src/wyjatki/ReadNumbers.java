package wyjatki;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadNumbers {
	
	public double readDouble(Scanner sc) {
		double in = 0.0;
		sc = new Scanner(System.in);
		System.out.println("Podaj liczb� typu double: ");
		try {
			in = sc.nextDouble();
		} catch(InputMismatchException e) {
			// nic nie r�b :)
		}
		return in;
	}
	
	public int readInt(Scanner sc) {
		int in = 0;
		sc = new Scanner(System.in);
		System.out.println("Podaj liczb� ca�kowit�: ");
		try {
			in = sc.nextInt();
		} catch(InputMismatchException e) {
			
		}
		return in;
	}
	
	public String readString(Scanner sc) {
		sc = new Scanner(System.in);
		System.out.println("Podaj �a�cuch znak�w: ");
		return sc.nextLine();
	}

}
