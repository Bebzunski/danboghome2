package wyjatki;

public class ExceptionSimpleClass {

	public void make(int a) throws IllegalArgumentException, Exception {
		if (a == 55)
			throw new IllegalArgumentException("niepoprawna liczba");
		if (a == 5)
			throw new Exception("niepoprawny argument");
		System.out.println("podana liczba jest OK");
	}
	
	public void exceptionExample(String name) throws DanielException {
		if(name.equalsIgnoreCase("daniel")) throw new DanielException("niepoprawne imi�");
		System.out.println("poprawne imi�");
	}

}
