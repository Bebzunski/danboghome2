package wyjatki;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		ExceptionSimpleClass test = new ExceptionSimpleClass();
		try {
			test.exceptionExample("daniel");
		} catch (DanielException e) {
			System.out.println(e.getMessage());
		}

		try {
			test.make(5);
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println("co to za dziwna liczba 5");
		} finally {
			System.out.println("[ END ]");
		}

//		int[] array = new int[3];
//		try(Scanner sc = new Scanner(System.in)){
//			for (int i = 0; i < 3; i++) {
//				System.out.println("Podaj array[" + i + "] = ");
//				array[i] = sc.nextInt();
//			}
//			
//		} catch (InputMismatchException e) {
//			System.out.println("niepoprawny argument");
//		};
//		
//		try {
//			System.out.println(array[10]);
//		} catch(ArrayIndexOutOfBoundsException e) {
//			System.out.println("Brak elementu pod indeksem: " + e.getMessage());
//		}
		
		
		try {
			System.out.println("Pierwiastek wynosi: " + Square.square(-4));
		} catch(IllegalArgumentException e) {
			System.out.println("Podano nieprawid�owy argument: " + e.getMessage());
		}
		
		
		
		// Zadanie 3.
		
		try {
			System.out.println(Division.divide(5, 0));
		} catch(IllegalArgumentException e) {
			System.out.println("Wystapil wyjatek: " + e.getMessage());
		}
		
		// Zadanie 4.
		
		ReadNumbers rn = new ReadNumbers();
		Scanner sc = new Scanner(System.in);
		System.out.println("Wczytana liczba zmiennoprzecinkowa to: "+ rn.readDouble(sc));
		
		System.out.println("Wczytana liczba ca�kowita to: " + rn.readInt(sc));
		
		System.out.println("Wczytany �a�cuch znak�w to: " + rn.readString(sc));
		sc.close();
		System.exit(0);
	}

}
