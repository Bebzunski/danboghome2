package wyjatki;

public class PeopleExample {

	public static void main(String[] args) {

		People p = new People(2);

		try {
			p.addPerson(new Person("a", "b", 1, "c", "d", 12.5));
			p.addPerson(new Person("aa", "bb", 11, "cc", "dd", 112.4));
			p.addPerson(new Person("aaa", "bbb", 111, "ccc", "ddd", 1112.5));
		} catch (WrongAgeException e) {
			System.out.println("Nastapil wyjatek");
		}

		for (Person person : p.getPpl()) {
			System.out.println(person);
		}
		
		System.exit(0);

		People ludzie = new People(3);
		System.out.println(ludzie.isFull());
		try {
			ludzie.addPerson(new Person("daniel", "bogdalski", 30, "blond", "brown", 45));
			ludzie.addPerson(new Person("daniel", "bogdalski", 31, "blond", "brown", 45));
			ludzie.addPerson(new Person("daniel", "bogdalski", 32, "blond", "brown", 45));
			try {
				ludzie.addPerson("kasia", "nowak", 134, "red", "blue", 42);
			} catch (BadHairException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BadEyesException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BadShoeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (WrongAgeException e) {
			System.out.println(e.getMessage());
		}
		System.out.println(ludzie.getPpl().get(0));
		System.out.println(ludzie.getPpl().get(1));
		System.out.println(ludzie.getPpl().get(2));

	}

}
