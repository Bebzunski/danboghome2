package wyjatki;

public class Division {
	
	public static double divide(int meter, int denominator) throws IllegalArgumentException {
		if(denominator == 0) throw new IllegalArgumentException("nie dzielimy przez zero");
		return meter/(double) denominator;
	}
	
	public static void main(String[] args) {
		try{
			System.out.println(divide(11,0));
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
		
	}

}
