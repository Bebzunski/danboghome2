package wyjatki;

public class WrongAgeException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WrongAgeException(String message){
		super(message);
	}
}
