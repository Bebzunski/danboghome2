package adnotacje;

public class AdnotacjeExample {

		@MojaAdnotacja
		private int i;
		
		public static void main(String[] args) {
			
			oldMethod();
		}
		
		@Deprecated
		@SuppressWarnings("unused")
		public static void oldMethod() {
					
			if (3>4) {
				System.out.println("OK");
			}
		}

}
