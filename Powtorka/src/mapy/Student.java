package mapy;

public class Student {
	
	private final int indexNumber;
	private final String name;
	private final String surname;
	
	public Student(int indexNumber, String name, String surname) {
		this.indexNumber = indexNumber;
		this.name = name;
		this.surname = surname;
	}

	public int getIndexNumber() {
		return indexNumber;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}
	

}
