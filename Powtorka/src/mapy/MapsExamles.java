package mapy;

import java.util.HashMap;
import java.util.Map;

public class MapsExamles {

	public static void main(String[] args) {
		
		Map<String, String> slownik = new HashMap<>();
		slownik.put("Kot", "Cat");
		slownik.put("Pies", "Dog");
		
		System.out.print("Tłumacznie 'Kot' : ");
		System.out.println(slownik.get("Kot"));
		
		System.out.println(slownik.get("Pies"));
		System.out.print("Klucze :");
		for(String key : slownik.keySet())
		System.out.print(" "+key);
		
		System.out.println();
		System.out.println("Pary");
		for(Map.Entry<String, String> para : slownik.entrySet()){
			System.out.println(para.getKey()+" na "+para.getValue());
			System.out.println(para);
		}
		
		slownik.remove("Pies");
		System.out.println("Pary klucz-wartosc po usunieciu psa");
		for (Map.Entry<String, String> pair : slownik.entrySet()) {
			System.out.println(pair);
		}
		System.out.println(slownik.get("Pies"));
		System.out.println(slownik.getOrDefault("Kot","Brak Tłumaczenia"));
	}

}
