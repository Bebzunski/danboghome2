package lista;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mapy.Person;

public class SortowanieListy {
	
	public static void main(String[] args) {


		List<Person> persons = new ArrayList<>();
		persons.add(new Person("Jan", 16));
		persons.add(new Person("Anna", 15));
		persons.add(new Person("Janina", 30));
		persons.add(new Person("Eustachy", 23));
		
		System.out.println("Przed sortowaniem");
		for (Person person : persons) {
			System.out.println(person);
		}

		Collections.sort(persons);
		
		System.out.println("Po sortowaniu");
		for (Person person : persons) {
			System.out.println(person);
		}
		
		Collections.shuffle(persons);
		System.out.println("Po tasowaniu");
		for (Person person : persons) {
			System.out.println(person);
		}
	}

}
