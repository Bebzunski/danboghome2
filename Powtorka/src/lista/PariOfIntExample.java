package lista;

import java.util.HashSet;
import java.util.Set;

public class PariOfIntExample {
	
	public static void main(String[] args) {


		Set<PairOfInt> pairs = new HashSet<>();
		
		PairOfInt a = new PairOfInt(1, 2);
		PairOfInt b = new PairOfInt(1, 2);
		System.out.println(a.equals(b));
		System.out.println(a.hashCode());
		System.out.println(b.hashCode());
		pairs.add(a);
		pairs.add(new PairOfInt(2, 1));
		pairs.add(new PairOfInt(1, 1));
		pairs.add(b);
		
		for (PairOfInt X : pairs) {
			System.out.println(X);
		}
	}

}
