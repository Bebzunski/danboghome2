package lista;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class SetsExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] tab = { 5000, 2000, 1000, 3000, 4000, 2000, 2000, 3000, 2000, 4000, 1000 };

		Set<Integer> set = new TreeSet<>();

		// Set<Integer> set = new
		// HashSet<>(Arrays.asList(1,2,1,3,4,2,2,3,2,4,5));

		for (int i : tab) {
			set.add(i);
		}

		System.out.println("Rozmiar zbioru : " + set.size());
		System.out.println("Zbi�r :");
		for (int i : set) {
			System.out.println(i);
		}


		System.out.println("Iterable :");

		Iterable<Integer> iterable = set;
		for (int i : iterable) {
			System.out.println(i);
		}
		
		System.out.println("Ca�y zbi�r :");
		Iterator<Integer> it = set.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}
}
