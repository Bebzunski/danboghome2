package lista;

import java.util.Arrays;
import java.util.List;

public class ListAndArray {
	
	public static void main(String[] args) {
		int[] array = {4,2,2,1,5,29,3};
		List<Integer> list = Arrays.asList(1,2,4,2,5,12,4);
		
		System.out.println(countDuplicates(array, list));
		
		List<Integer> lista1 = Arrays.asList(1,2,3);
		System.out.println(lista1.get(2));
		int[][] dwu = new int[3][4];
		System.out.println(dwu.length);
	}
	
	public static int countDuplicates(int[] array, List<Integer> list) {
		int count = 0;

		if (array.length != list.size()) {
			throw new RuntimeException("Kolekcje s� r�nej d�ugo�ci array : " + array.length + " list : "+ list.size());
		}
		
		for(int i =0 ; i < array.length; i++) {
			if (array[i] == list.get(i)) {
				count++;
			}
		}
		
		
		return count;
	}

}
