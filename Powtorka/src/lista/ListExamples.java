package lista;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListExamples {
	
public static void main(String[] args) {
		
		List<Integer> numbers = new ArrayList<Integer>(Arrays.asList(4,5,6));
		System.out.println("Ca�a lista :");
		for(int i : numbers) {
			System.out.println(i);
		}


		System.out.println("Element o indeksie 2 :");
		System.out.println(numbers.get(2));
		numbers.remove((Integer) 5);

		System.out.println("Ca�a lista :");
		for(int i : numbers) {
			System.out.println(i);
		}
		
		numbers.add(123);
		
		System.out.println("Ca�a lista :");
		for(int i : numbers) {
			System.out.println(i);
		}

		System.out.println("Rozmiar listy :");
		System.out.println(numbers.size());
	}

}
