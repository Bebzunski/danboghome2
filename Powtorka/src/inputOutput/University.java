package inputOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class University {

	private Map<Integer, Student> students = new HashMap<>();

	public boolean isStudentExists(int index) {
		return students.containsKey(index);
	}

	public Student getStudent(int index) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("resources/students.txt"));
		String[] splitedCurrentLine;
		int currentIndex;
		while (sc.hasNextLine()) {
			splitedCurrentLine = sc.nextLine().split("\t");
			currentIndex = Integer.parseInt(splitedCurrentLine[0].trim());
			if (currentIndex == index) {
				String name = splitedCurrentLine[1].trim();
				String secondName = splitedCurrentLine[2].trim();
				String course = splitedCurrentLine[3].trim();
				double averageMark = Double.parseDouble(splitedCurrentLine[4].trim());
				sc.close();
				return new Student(currentIndex, name, secondName, course, averageMark);
			}
		}
		sc.close();
		return null;
	}

	public void putStudent(Student student) {
		students.put(student.getIndex(), student);
	}

}
