package inputOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class TempConverter {

	private static String PATH = "src/";

	public double[] readTemp(String filename) {

		File f = new File(PATH + filename);
		double[] temp = new double[countLines(filename)];
		String currentLine;
		try (Scanner sc = new Scanner(f)) {
			int i = 0;
			while (sc.hasNextLine()) {
				currentLine = sc.nextLine().replace(',', '.');
				temp[i] = Double.parseDouble(currentLine);
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return temp;
	}

	private static int countLines(String filename) {	
		File f = new File(PATH + filename);
		int count = 0;
		try (Scanner sc = new Scanner(f)) {
			while (sc.hasNextLine()) {
				sc.nextLine();
				count++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return count;

	}

	public double toKelvin(double c) {
		return c + 273.15;
	}

	public double toFahrenheit(double c) {
		return (c * 1.8) + 32;
	}

	public void writeTemp(double[] temp) {
		File toK = new File(PATH + "tempK.txt");
		File toF = new File(PATH + "tempF.txt");

		try {
			FileOutputStream fosK = new FileOutputStream(toK);
			FileOutputStream fosF = new FileOutputStream(toF);

			PrintWriter pwK = new PrintWriter(fosK);
			PrintWriter pwF = new PrintWriter(fosF);

			for (double c : temp) {
				pwK.println(toKelvin(c));
				pwF.println(toFahrenheit(c));
			}

			pwK.close();
			pwF.close();

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

}
