package inputOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Sentence {

	public String readSentence(String filename) {
		String sentence = "";
		File f = new File("src/" + filename);
		try (Scanner sc = new Scanner(f)) {
			while(sc.hasNextLine()){
				sentence+=sc.nextLine();
			}
			sentence = sentence.toUpperCase().charAt(0)+sentence.substring(1)+".";
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sentence;
	}

}
