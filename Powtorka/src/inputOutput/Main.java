package inputOutput;

import java.io.FileNotFoundException;

public class Main {

	public static void main(String[] args) {

		Columner col = new Columner("columns.txt");
		col.readColumn(1);
		for(double num : col.getCurrentColumn()){
			System.out.println(num);
		}
		System.exit(0);

		AvgChecker avg = new AvgChecker("marks.txt");
		avg.process();

		University u = new University();
		try {
			System.out.println(u.getStudent(1));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		LengthChecker lc = new LengthChecker();
		lc.make("words.txt", 14);

		MoneyConverter mc = new MoneyConverter();
		System.out.println(mc.readCourse("tHb"));
		TempConverter tc = new TempConverter();
		double[] temperatury = tc.readTemp("temp.txt");
		for (double temp : temperatury) {
			System.out.println(temp);
		}

		tc.writeTemp(temperatury);

		Sentencer zdanie = new Sentencer();
		String sentence = zdanie.readSentence("test.txt").substring(15);
		zdanie.writeSentence("nowyPlik2.txt", sentence);

	}
}
