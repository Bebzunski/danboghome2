package inputOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Columner {

	private final String path = "resources/";
	private String filename;
	private List<String> fileContent = new LinkedList<>();
	private double[] currentColumn;

	public Columner(String filename) {
		super();
		this.filename = filename;
	}

	public void readColumn(int column) {
		readFile();
		String currentNumber;
		int maxColumn = fileContent.get(0).split("\t").length;
		if (column >= 1 && column <= maxColumn) {
			currentColumn = new double[fileContent.size()];
			for (int i = 0; i < fileContent.size(); i++) {
				String[] splited = fileContent.get(i).split("\t");
				currentNumber = splited[column - 1].replace(',', '.');
				currentColumn[i] = Double.parseDouble(currentNumber);
			}
		}
	}

	private void readFile() {

		if (fileContent.size() == 0) {

			File f = new File(path + filename);
			try (Scanner sc = new Scanner(f)) {
				while (sc.hasNextLine()) {
					fileContent.add(sc.nextLine());
				}

			} catch (FileNotFoundException e) {
				e.getStackTrace();
			}
		}
	}

	public double[] getCurrentColumn() {
		return currentColumn;
	}
	
	
}
