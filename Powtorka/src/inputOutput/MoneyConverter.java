package inputOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MoneyConverter {

	private final String filename = "src/currency.txt";

	public double readCourse(String currency) {

		double ret = 0.0;
		File f = new File(filename);
		String currentLine;

		try (Scanner sc = new Scanner(f)) {
			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				String[] pair = currentLine.split("\t");
				String[] value = pair[0].split(" ");
				String marka = value[1];
				double number = Double.parseDouble(value[0]);
				if (currency.equalsIgnoreCase(marka)) {
					String temp = pair[1].replace(',', '.');
					ret = Double.parseDouble(temp)/number;
					break;
				}
			}
		} catch (FileNotFoundException e) {
			e.getStackTrace();
		}
		return ret;
	}
}
