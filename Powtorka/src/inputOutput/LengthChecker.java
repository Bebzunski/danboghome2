package inputOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {

	private final String path = "resources/";
	
	public void make(String fileInput, int minLenght){
		String[] words = readFile(fileInput);
		writeFile(words, minLenght);
	}

	public String[] readFile(String filename) {

		File f = new File(path + filename);
		String[] words = new String[countLines(filename)];
		try (Scanner sc = new Scanner(f)) {
			for (int i = 0; i < words.length; i++) {
				words[i] = sc.nextLine().trim();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return words;
	}

	public void writeFile(String[] fileContent, int minLenght) {

		File f = new File(path + "words_length_min_" + minLenght + ".txt");
		try {
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			for (String element : fileContent) {
				if (isProperLength(element, minLenght)) {
					pw.println(element);
				}
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private boolean isProperLength(String word, int minLenght) {
		return word.length() >= minLenght;
	}

	private int countLines(String filename) {
		File f = new File(path + filename);
		int count = 0;
		try (Scanner sc = new Scanner(f)) {
			while (sc.hasNextLine()) {
				sc.nextLine();
				count++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return count;

	}

}
