package inputOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class AvgChecker {

	private final String path = "resources/";
	private String filename;

	public AvgChecker(String filename) {
		super();
		this.filename = filename;
	}

	public void process() {

		File f = new File(path + filename);
		List<String> sentences = new LinkedList<>();
		String currentLine;
		double sum = 0;
		try (Scanner sc = new Scanner(f)) {
			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				sentences.add(currentLine);
				sum += countAvgFromLine(currentLine);
			}

			double avg = sum / sentences.size();
			double roundAvg = ((int) (avg * 100)) / 100.0;
			FileOutputStream fos = new FileOutputStream(new File(path + "new_" + filename));
			PrintWriter pw = new PrintWriter(fos);
			pw.println("Studenci z wy�sz� �rednia ni� " + roundAvg + " :");
			for (String marks : sentences) {
				if (countAvgFromLine(marks) > avg) {
					pw.println(marks);
				}
			}
			pw.close();

		} catch (FileNotFoundException e) {
			// TODO: handle exception
		}

	}

	private double countAvgFromLine(String line) {

		String[] splited = line.split("\t");
		double sum = 0;
		for (int i = 1; i < splited.length; i++) {
			sum += Double.parseDouble(splited[i]);
		}
		return sum / (splited.length - 1);
	}

}
