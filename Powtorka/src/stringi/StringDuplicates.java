package stringi;

import java.util.HashSet;
import java.util.Set;

public class StringDuplicates {

	public static void main(String[] args) {
		
		System.out.println(ifDuplicates("asdfgh"));

	}
	
	static boolean ifDuplicates(String text){
		Set<Character> splited = new HashSet<>();
		for(Character letter : text.toCharArray()){
			splited.add(letter);
		}
		
		return splited.size() != text.length();
	}

}
