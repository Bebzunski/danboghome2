package stringi;

public class StringBuilderExample {
	
	
	
	public static void main(String[] args) {
		
		double length = 1000000;
		
		long start = System.currentTimeMillis();
		generateA((int) length);
		long stop = System.currentTimeMillis();
		System.out.println("Dla Stringa :"+ (stop - start)/1000.0+" sekund");
		
		start = System.currentTimeMillis();
		generateByBuilder(length);
		stop = System.currentTimeMillis();
		if((stop-start)<1000.0)
			System.out.println("Dla StringBuldera : "+ (stop - start)/1000.0+" sekundy");
		else
		System.out.println("Dla StringBuldera : "+ (stop - start)/1000.0+" sekund");
		
	}
	
	private static String generateByBuilder(double length) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0 ; i < length; i++){
			sb.append('a');
		}
		return sb.toString();
	}
	
	private static String generateA(int length) {
		String a = "";
		for(int i = 0 ; i < length; i++) {
			a = a +"a";
		}
		return a;
	}

}
