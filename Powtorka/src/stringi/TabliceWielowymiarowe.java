package stringi;

public class TabliceWielowymiarowe {

	public static void main(String[] args) {

		double[][] ints = new double[6][];

		System.out.println(ints[2]);

		ints[2] = new double[] { 1, 23, 2 };
		ints[3] = new double[] { 1, 2 };
		System.out.println(ints[2][1]);
		System.out.println(sum(ints[2]));

		System.out.println("Sumy : ");
		for (double i : sum2(ints)) {
			System.out.println(i);
		}
		System.exit(0);

		System.out.println(sum(new double[] { 4.5, 3, 6 }));

		for (int i = 0; i < ints.length; i++) {
			System.out.println(ints[i]);
			// System.out.println((int) wyniki[i]);
		}
	}

	public static double[] sum2(double[][] liczby) {

		double[] wyniki = new double[liczby.length];
		for (int i = 0; i < wyniki.length; i++) {
			if (liczby[i] != null) {
				wyniki[i] = sum(liczby[i]);
			}
		}

		return wyniki;
	}

	public static double sum(double[] liczby) {
		double suma = 0.0;
		for (double liczba : liczby) {
			suma += liczba;
		}
		return suma;
	}

}
