package stringi;

import java.util.Random;

public class StringUtil {
	private String str;

	public String getStr() {
		return str;
	}

	public StringUtil setStr(String str) {
		this.str = str;
		return this;
	}

	public StringUtil(String str) {
		this.str = str;
	}

	public static void main(String[] args) {
		StringUtil nowy = new StringUtil("Daniel");
		nowy.print();
		nowy.getAlphabet().print().reverse().print().swapLetters().print();
		nowy.resetText();
		nowy.setStr("Daniell Bogdalski").pokemon().print().getRandomHash(1).print();
		
	}

	public StringUtil print() {
		System.out.println(str);
		return this;
	}

	public StringUtil prepend(String arg) {
		str = arg + str;
		return this;
	}

	public StringUtil append(String arg) {
		str += arg;
		return this;
	}

	public StringUtil letterSpacing() {
		String[] letters = str.split("");
		str = "";
		for (String letter : letters) {
			str += letter + " ";
		}
		str = str.trim();
		return this;
	}

	public StringUtil reverse() {
		String[] letters = str.split("");
		str = "";
		for (int i = letters.length - 1; i >= 0; i--) {
			str += letters[i];
		}
		return this;
	}

	public StringUtil getAlphabet() {
		str = "";
		for (char c = 97; c <= 122; c++) {
			str += c;
		}
		return this;
	}

	public StringUtil getFirstLetter() {
		str = str.charAt(0) + "";
		return this;
	}

	public StringUtil limit(int n) {
		str = str.substring(0, n);
		return this;
	}

	public StringUtil insertAt(String string, int n) {
		str = str.substring(0, n) + string + str.substring(n);
		return this;
	}

	public StringUtil resetText() {
		str = "";
		return this;
	}

	public StringUtil swapLetters() {
		str = str.charAt(str.length() - 1) + str.substring(1, str.length() - 1) + str.charAt(str.length() - 1);
		return this;
	}

	public StringUtil createSentence() {
		str = (str.charAt(0) + "").toUpperCase() + str.substring(1);
		if (str.charAt(str.length() - 1) != '.') {
			str += ".";
		}
		return this;
	}

	public StringUtil cut(int from, int to) {
		String newString = "";
		for (int i = from; i <= to; i++) {
			newString += str.charAt(i);
		}
		str = newString;
		return this;
	}

	public StringUtil pokemon() {
		String[] splited = str.split("");
		str = "";
		for (int i = 0; i < splited.length; i++) {
			if (i % 2 == 1) {
				str += splited[i].toUpperCase();
			} else {
				str += splited[i].toLowerCase();
			}
		}
		return this;
	}
	
	public StringUtil getRandomHash(int n) {
		Random r = new Random();
		String ret = "";
		for(int i = 0; i < n; i++) {
			if(r.nextInt(2) == 1) {
				ret += (char) r.nextInt(6) + 97;
			} else {
				ret += (char) r.nextInt(10) + 48;
			}
		}
		str = ret;
		return this;
	}
}
