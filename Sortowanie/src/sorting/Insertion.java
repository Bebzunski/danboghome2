package sorting;

public class Insertion implements Sort{
	
	public int[] sort(int[] table) {
		int current;
		int j;
		for (int i = 1; i < table.length; i++) {
			current = table[i];
			j = i - 1;
			while (j >= 0 && current < table[j]) {
				table[j + 1] = table[j];
				j--;
			}
			table[j + 1] = current;
		}
		return table;
	}

}
