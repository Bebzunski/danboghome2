package sorting;

public class Selection implements Sort{
	
	public int[] sort(int[] table) {
		int index;
		int min;
		for (int i = 0; i < table.length - 1; i++) {
			index = findMinIndex(table, i);
			min = table[index];
			table[index] = table[i];
			table[i] = min;
		}
		return table;
	}

	public static int findMinIndex(int[] table, int from) {
		int min = table[from];
		int indexMin = from;
		for (int i = from + 1; i < table.length; i++) {
			if (table[i] < min) {
				min = table[i];
				indexMin = i;
			}
		}
		return indexMin;
	}

}
