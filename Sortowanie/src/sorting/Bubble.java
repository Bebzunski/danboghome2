package sorting;

public class Bubble implements Sort{
	
	public int[] sort(int[] table) {

		for (int j = table.length - 1; j > 0; j--) {
			for (int i = 0; i < j; i++) {
				int left = table[i];
				int right = table[i + 1];
				if (left > right) {
					table[i + 1] = left;
					table[i] = right;
				}
			}
		}
		return table;
	}

}
