package sorting;

public class Main {

	public static void main(String[] args) {

		int[] table = { 4, 1, -8, 14, 5, -7, 1, -3 };
		
		Sort mySort = new Selection();
		mySort.sort(table);
		Sort.printTable(table);

	}

}
