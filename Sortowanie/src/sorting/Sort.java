package sorting;

public interface Sort {

	int[] sort(int[] table);
	
	public static void printTable(int[] table) {
		for (int n : table) {
			System.out.print(n + " ");
		}
		System.out.println();
	}

}
