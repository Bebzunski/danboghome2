package exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	private static Scanner sc;

	public static void main(String[] args) {
			
		System.out.println(ReadNumbers.liczba);
		System.out.println(Math.PI);
		
		System.exit(0);
		
		try{
		System.out.println(Square.square(-9));
		} catch(IllegalArgumentException e){
			System.out.println(e.getMessage());
		}
		
		
		try{
		showElementOfArray(10 , 12);
		} catch (InputMismatchException e){
			System.out.println(e.getMessage());
		}
		
		sc.close();
	}

	private static void showElementOfArray(int length, int whichIndex) throws InputMismatchException{
		int[] arrayInt = new int[3];
		sc = new Scanner(System.in);
		System.out.println("Podaj "+" liczb ca�kowitych.");
		for(int i=0; i < arrayInt.length ; i++){
			arrayInt[i]=sc.nextInt();
			if(arrayInt[i]<0){
				throw new InputMismatchException("Podale� ujemn� liczb�");
			}
			
			System.out.println("arrayInt["+i+"] = "+arrayInt[i]);
		}
			
			
			try{
				System.out.println(arrayInt[whichIndex]);
			} catch (ArrayIndexOutOfBoundsException e){
				System.out.println("Poda�e� niepoprawny indeks: "+e.getMessage());
			}
			
			
	}
		
}
