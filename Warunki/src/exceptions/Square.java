package exceptions;

public class Square {
	
	public static double square(int n) throws IllegalArgumentException{
		if(n<0){
			throw new IllegalArgumentException("poda�es liczb� ujemn� pod pierwiastkiem");
		}
		return Math.sqrt(n);
	}

}
