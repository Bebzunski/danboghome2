package factory;

public class Game {
	
	private String name;
	private int bigBlind;
	private int players;
	private boolean turnament;
	private String limit;
	
	
	
	private Game(String name, int bigBlind, int players, boolean turnament, String limit) {
		super();
		this.name = name;
		this.bigBlind = bigBlind;
		this.players = players;
		this.turnament = turnament;
		this.limit = limit;
	}



	public String getName() {
		return name;
	}



	public int getBigBlind() {
		return bigBlind;
	}



	public int getPlayers() {
		return players;
	}



	public boolean isTurnament() {
		return turnament;
	}



	public String getLimit() {
		return limit;
	}
	
	
	@Override
	public String toString() {
		return "Game [name=" + name + ", bigBlind=" + bigBlind + ", players=" + players + ", turnament=" + turnament
				+ ", limit=" + limit + "]";
	}



	public static Game Game1(){
		return new Game("TexasHoldem", 10, 9, true, "NO LIMIT");
	}
	
	public static Game Game2(){
		return new Game("Omaha", 50, 6, false, "POT LIMIT");
	}

}
