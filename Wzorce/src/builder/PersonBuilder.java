package builder;

public class PersonBuilder {

	private String name;
	private String lastname;
	private int age;
	private double weight;
	private double height;

	public PersonBuilder() {

	}

	public String getName() {
		return name;
	}

	public PersonBuilder setName(String name) {
		this.name = name;
		return this;
	}

	public String getLastname() {
		return lastname;
	}

	public PersonBuilder setLastname(String lastname) {
		this.lastname = lastname;
		return this;
	}

	public int getAge() {
		return age;
	}

	public PersonBuilder setAge(int age) {
		this.age = age;
		return this;
	}

	public double getWeight() {
		return weight;
	}

	public PersonBuilder setWeight(double weight) {
		this.weight = weight;
		return this;
	}

	public double getHeight() {
		return height;
	}

	public PersonBuilder setHeight(double height) {
		this.height = height;
		return this;
	}

	public Person createPerson() {
		return new Person(name, lastname, age, weight, height);
	}

}
