package builder;

public class Main {

	public static void main(String[] args) {
		
		Person ja = new PersonBuilder().setName("Daniel").setAge(30).setLastname("Bogdalski").createPerson();
		System.out.println(ja);

	}

}
