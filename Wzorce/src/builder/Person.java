package builder;

public class Person {
	
	private String name;
	private String lastname;
	private int age;
	private double weight;
	private double height;
	
	public Person(String name, String lastname, int age, double weight, double height) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.age = age;
		this.weight = weight;
		this.height = height;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", lastname=" + lastname + ", age=" + age + ", weight=" + weight + ", height="
				+ height + "]";
	}
	
	

}
