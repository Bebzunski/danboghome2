package singleton;

public class Main {

	public static void main(String[] args) {
		
		MySingleton obiekt1 = MySingleton.createInstance();
		MySingleton obiekt2 = MySingleton.createInstance();
		System.out.println(obiekt1);
		System.out.println(obiekt2);
	
	}

}
