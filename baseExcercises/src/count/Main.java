package count;

public class Main {

	public static void main(String[] args) {
		
		final String ANSI_RED = "\u001B[31m";
		System.out.println(ANSI_RED+"fgdffhd");
		
		String c = "-----------------------------------------------------------------------";
		
		System.out.println(c.length());
		
		System.exit(0);
		
		System.out.println(NWD(56, 37));
		
		System.out.println(countField(2 , 5 , 6));

		System.exit(0);

		int[] array = { 4, 5, 1, -5, 2, 1 };

		Main m = new Main();

		System.out.println(m.sum(array));
		System.out.println(product(array));

		boolean[] arrayOfBooleans = { true, true, true, true, true, true };

		System.out.println(conjunction(arrayOfBooleans));
		System.out.println(alternative(arrayOfBooleans));
		System.out.println(silnia(3));

		sumOfArithmeticSeries(12, 2, 4);
		System.out.println(SumOfGeometricThrust(1, 1.0 / 2, 20));
		System.out.println(countChar('r', "rabarbarR"));

	}

	public int sum(int[] array) {

		int sum = 0;
		for (int num : array) {
			sum += num;
		}
		return sum;
	}

	public static int product(int[] array) {

		int product = 1;
		for (int num : array) {
			product *= num;
		}
		return product;
	}

	public static boolean conjunction(boolean[] array) {

		for (boolean num : array) {
			if (num == false) {
				return false;
			}
		}
		return true;
	}

	public static boolean alternative(boolean[] array) {

		for (boolean num : array) {
			if (num == true) {
				return true;
			}
		}
		return false;
	}

	public static int silnia(int n) throws IllegalArgumentException {

		if (n >= 0) {
			int ret = 1;
			for (int i = 2; i <= n; i++) {
				ret *= i;
			}
			return ret;
		} else {
			throw new IllegalArgumentException();
		}

	}

	private static int sumOfArithmeticSeries(int a1, int r, int n) {
		int a = a1;
		int sum = a1;
		for (int i = 2; i <= n; i++) {
			a = a + r;
			sum += a;
		}
		return sum;
	}

	private static double SumOfGeometricThrust(double a1, double q, int n) {
		double a = a1;
		double product = a1;
		for (int i = 2; i <= n; i++) {
			a = a * q;
			product += a;
		}
		return product;
	}

	private static int countChar(char c, String word) {

		int ret = 0;
		for (int i = 0; i < word.length(); i++) {
			if (("" + word.charAt(i)).equalsIgnoreCase("" + c)) {
				ret++;
			}
		}
		return ret;
	}

	private static double absoluteDifference(double a, double b) {
		if ((a - b) >= 0) {
			return a - b;
		} else {
			return b - a;
		}

	}

	private static int NWD(int a, int b) {
		int commoDivisor = 1;
		for (int i = 1; i <= absoluteDifference(a, b); i++) {
			if (a % i == 0 && b % i == 0) {
				commoDivisor = i;
			}
		}
		return commoDivisor;
	}
	
	public static double countField(double a, double b, double c){
		
		if(a+b>c && a+c>b && b+c > a){
		
		double h = Math.sqrt( b*b - ((a*a+b*b-c*c)*(a*a+b*b-c*c))/(4*a*a));
		return a*h/2;
		} else {
			return 0.0;
		}
	}
}
