package spoj;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class SorterPoints {

	public static void main(String[] args) {

		solve(new Scanner(System.in));
	}

	public static void solve(Scanner sc) {
		int numberOfTests = sc.nextInt();
		List<Set<Point>> results = new ArrayList<>();
		int numberOfPoints;
		for (int i = 0; i < numberOfTests; i++) {
			numberOfPoints = sc.nextInt();
			Set<Point> points = new TreeSet<>();
			for (int j = 0; j < numberOfPoints; j++) {
				points.add(new Point(sc.next(), sc.nextInt(), sc.nextInt()));
			}
			results.add(points);
		}
		showResults(results);
	}

	public static void showResults(List<Set<Point>> results) {
		for (int i = 0; i < results.size(); i++) {
			for (Point p : results.get(i)) {
				System.out.println(p);
			}
			if (i < results.size() - 1)
				System.out.println();
		}
	}

	static class Point implements Comparable<Point> {

		public String name;
		public int x;
		public int y;
		public int r;

		public Point(String name, int x, int y) {
			this.name = name;
			this.x = x;
			this.y = y;
			r = x * x + y * y;
		}

		@Override
		public String toString() {
			return name + " " + x + " " + y;
		}

		@Override
		public int compareTo(Point o) {
			if (this.r > o.r)
				return 1;
			else
				return -1;
		}

	}

}
