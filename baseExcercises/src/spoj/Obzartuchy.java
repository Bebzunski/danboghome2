package spoj;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Obzartuchy {

	public static List<Integer> results = new LinkedList<>();

	public static void main(String[] args) {

		solveTheProblem();

	}

	public static void solveTheProblem() {
		Scanner sc = new Scanner(System.in);
		int nrOfTests = Integer.parseInt(sc.nextLine());
		int numberOfPlayers;
		int allEatenCookies;
		int numberOfCookiesInOneBox;
		String[] currentLine = new String[2];
		for (int i = 0; i < nrOfTests; i++) {
			currentLine = sc.nextLine().split(" ");
			numberOfPlayers = Integer.parseInt(currentLine[0]);
			numberOfCookiesInOneBox = Integer.parseInt(currentLine[1]);
			allEatenCookies = 0;
			for (int j = 0; j < numberOfPlayers; j++) {
				allEatenCookies += howManyCookiesInDay(Integer.parseInt(sc.nextLine()));
			}
			results.add(howManyBoxes(numberOfCookiesInOneBox, allEatenCookies));
		}
		sc.close();
		for (Integer num : results) {
			System.out.println(num);
		}
	}

	public static int howManyBoxes(int inOneBox, int numberOfCookies) {
		return (int) Math.ceil(numberOfCookies / (double) inOneBox);
	}

	public static int howManyCookiesInDay(int rate) {
		return 86400 / rate;
	}

}
