package spoj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) {

		FastReader sc = new FastReader();
		int[][] inputs = new int[sc.nextInt()][];
		for (int i = 0; i < inputs.length; i++) {
			inputs[i] = new int[sc.nextInt()];
			for (int j = 0; j < inputs[i].length; j++) {
				inputs[i][j] = sc.nextInt();
			}
		}

		int[] maxAndHisNumber = new int[2];
		int[] sortedtable;
		for (int i = 0; i < inputs.length; i++) {
			maxAndHisNumber = countMax(inputs[i]);
			sortedtable = sort(inputs[i]);
			for(int j = 0 ; j < maxAndHisNumber[1] ; j++){
				System.out.print(maxAndHisNumber[0]+" ");
			}
			for(int j = 0 ; j < sortedtable.length - maxAndHisNumber[1] ; j++){
				System.out.print(sortedtable[j]+" ");
			}
			System.out.println();
		}

	}

	public static int[] sort(int[] table) {

		for (int j = table.length - 1; j > 0; j--) {
			for (int i = 0; i < j; i++) {
				int left = table[i];
				int right = table[i + 1];
				if (left > right) {
					table[i + 1] = left;
					table[i] = right;
				}
			}
		}
		return table;
	}

	public static int[] countMax(int[] row) {
		int max = row[0];
		int count = 1;
		for (int i = 1; i < row.length; i++) {
			if (row[i] == max)
				count++;
			if (row[i] > max) {
				max = row[i];
				count = 1;
			}
		}
		return new int[] { max, count };
	}

	static class FastReader {
		BufferedReader br;
		StringTokenizer st;

		public FastReader() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}

		double nextDouble() {
			return Double.parseDouble(next());
		}

		String nextLine() {
			String str = "";
			try {
				str = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;
		}

	}

}
