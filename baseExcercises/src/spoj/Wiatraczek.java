package spoj;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Wiatraczek {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		List<List<StringBuilder>> results = new ArrayList<>();
		int input = sc.nextInt();
		do {
			results.add(getFan(input));
			input = sc.nextInt();
		} while (input != 0);
		sc.close();
		
		for(List<StringBuilder> fan : results){
			printFan(fan);
		}
	}
	
	public static void printFan(List<StringBuilder> fan){
		for (StringBuilder s : fan) {
			System.out.println(s);
		}
		System.out.println();
	}

	public static List<StringBuilder> getFan(int dimension) {
		if (dimension < 0) {
			return getRightHandedWhirlingFan(-dimension);
		} else if (dimension > 0) {
			return getLeftHandedWhirlingFan(dimension);
		} else
			return new LinkedList<>();
	}

	public static List<StringBuilder> getLeftHandedWhirlingFan(int dimension) {

		List<StringBuilder> wiatraczek = new LinkedList<>();
		wiatraczek.add(new StringBuilder("**"));
		wiatraczek.add(new StringBuilder("**"));

		for (int n = 2; n <= dimension; n++) {
			makeSideWallFirstStar(wiatraczek);
			wiatraczek.add(0, makeLineFirstDot(n));
			wiatraczek.add(makeLineFirstStar(n));
		}
		return wiatraczek;
	}

	public static List<StringBuilder> getRightHandedWhirlingFan(int dimension) {

		List<StringBuilder> wiatraczek = new LinkedList<>();
		wiatraczek.add(new StringBuilder("**"));
		wiatraczek.add(new StringBuilder("**"));

		for (int n = 2; n <= dimension; n++) {
			makeSideWallFirstDot(wiatraczek);
			wiatraczek.add(0, makeLineFirstStar(n));
			wiatraczek.add(makeLineFirstDot(n));
		}
		return wiatraczek;
	}

	public static void makeSideWallFirstDot(List<StringBuilder> wiatraczek) {
		for (int i = 0; i < wiatraczek.size() / 2; i++) {
			appendFirstDotLastStar(wiatraczek.get(i));
		}
		for (int i = wiatraczek.size() / 2; i < wiatraczek.size(); i++) {
			appendFirstStarLastDot(wiatraczek.get(i));
		}
	}

	public static void makeSideWallFirstStar(List<StringBuilder> wiatraczek) {
		for (int i = 0; i < wiatraczek.size() / 2; i++) {
			appendFirstStarLastDot(wiatraczek.get(i));
		}
		for (int i = wiatraczek.size() / 2; i < wiatraczek.size(); i++) {
			appendFirstDotLastStar(wiatraczek.get(i));
		}
	}

	public static StringBuilder makeLineFirstDot(int dimension) {
		StringBuilder line = new StringBuilder();
		for (int i = 0; i < dimension - 1; i++) {
			appendFirstDotLastStar(line);
		}
		line.insert(0, "*");
		line.append("*");
		return line;
	}

	public static StringBuilder makeLineFirstStar(int dimension) {
		StringBuilder line = new StringBuilder();
		for (int i = 0; i < dimension - 1; i++) {
			appendFirstStarLastDot(line);
		}
		line.insert(0, "*");
		line.append("*");
		return line;
	}

	public static void appendFirstDotLastStar(StringBuilder arg) {
		arg.insert(0, ".");
		arg.append("*");
	}

	public static void appendFirstStarLastDot(StringBuilder arg) {
		arg.insert(0, "*");
		arg.append(".");
	}

}
