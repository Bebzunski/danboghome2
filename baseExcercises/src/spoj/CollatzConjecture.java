package spoj;

import java.util.Scanner;

public class CollatzConjecture {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int[] results = new int[sc.nextInt()];
		for (int i = 0; i < results.length; i++) {
			results[i] = getFirstElementEqualsOne(sc.nextInt());
		}
		sc.close();
		for (int result : results) {
			System.out.println(result);
		}

	}

	public static int getFirstElementEqualsOne(int firstElement) {
		int currentElement = firstElement;
		int index = 0;
		while (currentElement != 1) {
			if (currentElement % 2 == 1) {
				currentElement = 3 * currentElement + 1;
			} else {
				currentElement = currentElement / 2;
			}
			index++;
		}
		return index;
	}

}
