package spoj;

import java.util.Scanner;

public class Newton {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		double[] results = new double[sc.nextInt()];
		for (int i = 0; i < results.length; i++) {
			results[i] = newton(sc.nextInt(), sc.nextInt());
		}
		sc.close();
		for (double res : results) {
			System.out.println(res);
		}

	}

	public static double newton(int n, int k) {
		if (n == k || n == 0)
			return 1;

		k = Math.min(k, n - k);
		int[] licznik = malejaceLiczby(n, k);
		int[] mianownik = rosnaceLiczby(k);

		double result = 1;
		int p = 0;
		int q = 0;

		while (p < licznik.length || q < mianownik.length) {
			if (p < licznik.length && (result * licznik[p]) <= 1000000000) {
				result *= licznik[p];
				p++;
			} else {
				result /= mianownik[q];
				q++;
			}
		}
		return result;
	}

	public static int[] malejaceLiczby(int start, int dlugosc) {
		int[] result = new int[dlugosc];
		for (int i = 0; i < dlugosc; i++) {
			result[i] = start - i;
		}
		return result;
	}

	public static int[] rosnaceLiczby(int ostatnia) {
		int[] result = new int[ostatnia - 1];
		for (int i = 0; i < result.length; i++) {
			result[i] = i + 2;
		}
		return result;
	}

}
