package spoj;

import java.util.Scanner;

public class Przedszkolanka {

	public static void main(String[] args) {
		nursery();
	}

	public static void nursery() {
		Scanner sc = new Scanner(System.in);
		int number = Integer.parseInt(sc.nextLine());
		int[][] pairOfNumbers = new int[number][2];
		String[] currentLine;
		for (int i = 0; i < number; i++) {
			currentLine = sc.nextLine().trim().split(" ");
			pairOfNumbers[i][0] = Integer.parseInt(currentLine[0]);
			pairOfNumbers[i][1] = Integer.parseInt(currentLine[1]);
		}
		sc.close();
		for (int i = 0; i < pairOfNumbers.length; i++) {
			System.out.println(nWW(pairOfNumbers[i][0], pairOfNumbers[i][1]));
		}
	}

	private static int nWW(int a, int b) {

		int multiA = a;
		int multiB = b;

		while (multiA != multiB) {

			if (multiA < multiB) {
				multiA += a;
			} else {
				multiB += b;
			}
		}

		return multiA;
	}

}
