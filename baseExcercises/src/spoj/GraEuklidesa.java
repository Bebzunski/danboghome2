package spoj;

import java.util.Scanner;

public class GraEuklidesa {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int[] results = new int[Integer.parseInt(sc.nextLine())];
		Chips chips = new Chips();
		String[] currentLine;
		for (int i = 0; i < results.length; i++) {
			currentLine = sc.nextLine().split(" ");
			chips.a = Integer.parseInt(currentLine[0]);
			chips.b = Integer.parseInt(currentLine[1]);
			while (chips.a != chips.b) {
				chips.play();
			}
			results[i] = chips.getSum();
		}
		sc.close();

		for (int num : results) {
			System.out.println(num);
		}
	}

	static class Chips {

		int a;
		int b;

		public Chips() {
		}

		public Chips(int a, int b) {
			this.a = a;
			this.b = b;
		}

		public int getSum() {
			return a + b;
		}

		public void play() {
			if (a > b) {
				a -= b;
			} else if (b > a) {
				b -= a;
			}
		}
	}

}
