package spoj;

import java.util.Scanner;

public class NWD {

	public static void main(String[] args) {
		System.out.println(greatestCommonDivisor(0, 45));
		solveTheProblem(new Scanner(System.in));
	}

	public static void solveTheProblem(Scanner sc) {
		int[] results = new int[sc.nextInt()];
		for(int i = 0 ; i < results.length ; i++){
			results[i] = greatestCommonDivisor(sc.nextInt(), sc.nextInt());
		}
		
		for(int i = 0 ; i < results.length ; i++){
			System.out.println(results[i]);
		}
	}

	public static int greatestCommonDivisor(int first, int second) {
		int bigger = Math.max(first, second);
		int smaller = Math.min(first, second);
		if (smaller == 0) {
			return bigger;
		}
		int difference;
		while (bigger != smaller) {
			difference = bigger - smaller;
			bigger = Math.max(smaller, difference);
			smaller = Math.min(smaller, difference);
		}
		return smaller;
	}
}
