package spoj;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SkarbFinder {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int numberOfTests = sc.nextInt();
		List<Skarb> results = new ArrayList<>(numberOfTests);
		int steps;
		Skarb skarb;
		for (int i = 0; i < numberOfTests; i++) {
			steps = sc.nextInt();
			skarb = new Skarb();
			for (int j = 0; j < steps; j++) {
				skarb.changePosition(sc.nextInt(), sc.nextInt());
			}
			results.add(i, skarb);
		}
		sc.close();

		for (Skarb result : results) {
			System.out.println(result);
		}

	}

	static class Skarb {

		int north = 0;
		int east = 0;

		public void changePosition(int direction, int steps) {
			switch (direction) {
			case 0:
				north += steps;
				break;
			case 1:
				north -= steps;
				break;
			case 2:
				east -= steps;
				break;
			case 3:
				east += steps;
				break;
			default:
				break;
			}
		}

		@Override
		public String toString() {
			if (north == 0 && east == 0) {
				return "studnia";
			}
			if (east == 0) {
				return getNorthSouthDirection();
			}
			if (north == 0) {
				return getEastWestDirection();
			}
			return getNorthSouthDirection() + "\n" + getEastWestDirection();
		}

		private String getNorthSouthDirection() {
			if (north < 0) {
				return "1 " + (-north);
			}
			return "0 " + north;
		}

		private String getEastWestDirection() {
			if (east < 0) {
				return "2 " + (-east);
			}
			return "3 " + east;
		}
	}

}
