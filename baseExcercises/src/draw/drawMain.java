package draw;

public class drawMain {

	public static void main(String[] args) {
		drawRectangle(5,7);
		
	}

	private static String drawLine(int length) {

		String line = "";
		for (int i = 1; i <= length; i++) {
			line += " *";
		}
		return (line+"\n");
	}

	private static void drawRectangle(int length, int height) {
		
		String rectangle = "";
		for(int i=1 ; i<=height ; i++){
		rectangle+=drawLine(length);
		}
		System.out.println(rectangle);
	}
}
