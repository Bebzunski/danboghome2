package wyjatki;

import java.util.Scanner;

public class Z17_1 {

	public static void main(String[] args) {

		System.out.println("Dzielenie z reszt�");
		Scanner input = new Scanner(System.in);
		System.out.print("m = ");
		String sm = input.next();
		System.out.print("n = ");
		String sn = input.next();
		try {
			int m = Integer.parseInt(sm);
			int n = Integer.parseInt(sn);
			System.out.printf("%d:%d = %d reszta %d\n", m, n, m / n, m % n);
		} catch (NumberFormatException e) {
			System.out.println("B��d podczas wprowadzania danych!");
		} catch (ArithmeticException e) {
			e.getStackTrace();
		}
		input.close();
	}

}
