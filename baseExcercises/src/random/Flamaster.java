package random;

import java.util.Scanner;

public class Flamaster {

	public static void main(String[] args) {
		System.out.println(change("ADSFGDDDSFG"));
		System.out.println(change("ADSFGDDDSFGG"));
		System.out.println(change("ADSFGDDDSFGGG"));
	}

	public static void flamaster() {
		Scanner sc = new Scanner(System.in);
		int numberOfWords = Integer.parseInt(sc.nextLine());
		String[] words = new String[numberOfWords];
		for (int i = 0; i < numberOfWords; i++) {
			words[i] = sc.nextLine().trim();
		}
		sc.close();
		for (int i = 0; i < numberOfWords; i++) {
			System.out.println(change(words[i]));
		}
	}

	public static String change(String word) {

		StringBuilder result = new StringBuilder();
		String currentLetter = "" + word.charAt(0);
		int count = 1;
		for (int i = 1; i < word.length(); i++) {
			if (currentLetter.equals("" + word.charAt(i))) {
				count++;
			} else if (count == 1) {
				result.append(currentLetter);
				currentLetter = "" + word.charAt(i);
				count = 1;
			} else if (count == 2) {
				result.append(currentLetter);
				result.append(currentLetter);
				currentLetter = "" + word.charAt(i);
				count = 1;
			} else {
				result.append(currentLetter + count);
				currentLetter = "" + word.charAt(i);
				count = 1;
			}
		}
		
		if(count == 1)
			result.append(currentLetter);
		else if (count == 2)
			result.append(currentLetter + currentLetter);
		else
			result.append(currentLetter + count);

		return result.toString();
	}

}
