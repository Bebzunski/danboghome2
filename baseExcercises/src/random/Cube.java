package random;

import java.util.Random;

public class Cube {

	public static void main(String[] args) {
		
		int[] count = new int[13];
		for(int i = 0 ; i < 10000000 ; i++){
			countNumber(count, rollDice() + rollDice());
		}
		printResult(count);

	}

	public static int rollDice() {
		return new Random().nextInt(6) + 1;
	}

	public static void countNumber(int[] count, int number) {
		if (number < 1 || number >= count.length) {
			throw new IllegalArgumentException();
		}
		count[0]++;
		count[number]++;
	}

	public static void printResult(int[] results) {
		System.out.println("Ilo�� pr�b : " + results[0]);
		for (int i = 1; i < results.length; i++) {
			System.out.println(i + " : " + (results[i]*100.0)/results[0] + "%");
		}
	}

}
