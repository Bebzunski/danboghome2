package program;

import java.util.List;

public class MyComparator {
	
	private int sample = 0;
	private int goodAnswers = 0;	

	public boolean[] compare(List<String> answers, List<String> results) {
		boolean[] compareResults = new boolean[results.size()];
		if (answers.size() != results.size() || answers == null || results == null) {
			return compareResults;
		}
		sample++;
		for (int i = 0; i < results.size(); i++) {
			compareResults[i] = (results.get(i).trim().equalsIgnoreCase(answers.get(i).trim()));
		}
		areAllCorrectAnswers(compareResults);
		return compareResults;
	}

	private boolean areAllCorrectAnswers(boolean[] results) {
		for (int i = 0; i < results.length; i++) {
			if (results[i] == false) {
				return false;
			}
		}
		goodAnswers++;
		return true;
	}

	public int getSample() {
		return sample;
	}

	public void setSample(int sample) {
		this.sample = sample;
	}

	public int getGoodAnswers() {
		return goodAnswers;
	}

	public void setGoodAnswers(int goodAnswers) {
		this.goodAnswers = goodAnswers;
	}

}
