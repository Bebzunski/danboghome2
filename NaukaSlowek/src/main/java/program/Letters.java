package program;

public class Letters {

	private static final char[] letters = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
			'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

	public static boolean isFirstLetterInRange(String word, char first, char last) {

		if (!Character.isLetter(word.charAt(0))) {
			return false;
		}

		if (!Character.isLetter(first)) {
			return false;
		}

		if (!Character.isLetter(last)) {
			return false;
		}

		first = Character.toLowerCase(first);
		last = Character.toLowerCase(last);

		if (first > last) {
			return false;
		}

		int firstIndex = findIndex(first);
		int lastIndex = findIndex(last);

		for (int i = firstIndex; i <= lastIndex; i++) {
			if (letters[i] == Character.toLowerCase(word.charAt(0))) {
				return true;
			}
		}

		return false;
	}

	private static Integer findIndex(char letter) {

		for (int i = 0; i < letters.length; i++) {
			if (letters[i] == letter) {
				return i;
			}
		}
		return null;
	}
}
