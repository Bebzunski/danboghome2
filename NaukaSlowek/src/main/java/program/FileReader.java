package program;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class FileReader {

	private static final String path = "src/main/resources/";
	private File file;
	private Scanner scanner;
	private int amount;

	private List<String> infinitiveList;
	private List<String> pastTenseList;
	private List<String> pastParticipleList;
	private List<String> polskeZnaczenieList;

	public FileReader(String filename) {
		this.file = new File(path + filename);
	}

	public void load(int option, boolean isRandom) {

		infinitiveList = new ArrayList<String>();
		pastTenseList = new ArrayList<String>();
		pastParticipleList = new ArrayList<String>();
		polskeZnaczenieList = new ArrayList<String>();
		amount = 0;

		List<String> sentences = new ArrayList<String>();
		String[] splitedCurrentLine = new String[4];

		char[][] ranges = { { 'a', 'b' }, { 'c', 'e' }, { 'f', 'h' }, { 'i', 'r' }, { 's', 's' }, { 't', 'z' },
				{ 'a', 'z' } };

		try {
			scanner = new Scanner(file);
			String currentLine;
			while (scanner.hasNextLine()) {
				currentLine = scanner.nextLine();
				if (Letters.isFirstLetterInRange(currentLine, ranges[option][0], ranges[option][1])) {
					sentences.add(currentLine);
					amount++;
				}
			}
			scanner.close();

			if (isRandom) {
				sentences = random(sentences);
			}

			for (int i = 0; i < sentences.size(); i++) {
				splitedCurrentLine = sentences.get(i).split("\t");
				infinitiveList.add(splitedCurrentLine[0]);
				pastTenseList.add(splitedCurrentLine[1]);
				pastParticipleList.add(splitedCurrentLine[2]);
				polskeZnaczenieList.add(splitedCurrentLine[3]);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<String> random(List<String> orderedWords) {

		Random r = new Random();
		List<String> randomWords = new ArrayList<String>();
		int size = orderedWords.size();

		for (int j = 0; j < size; j++) {
			int randomIndex = r.nextInt(orderedWords.size());
			randomWords.add(orderedWords.remove(randomIndex));
		}
		return randomWords;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public List<String> getInfinitiveList() {
		return infinitiveList;
	}

	public void setInfinitiveList(List<String> infinitiveList) {
		this.infinitiveList = infinitiveList;
	}

	public List<String> getPastTenseList() {
		return pastTenseList;
	}

	public void setPastTenseList(List<String> pastTenseList) {
		this.pastTenseList = pastTenseList;
	}

	public List<String> getPastParticipleList() {
		return pastParticipleList;
	}

	public void setPastParticipleList(List<String> pastParticipleList) {
		this.pastParticipleList = pastParticipleList;
	}

	public List<String> getPolskeZnaczenie() {
		return polskeZnaczenieList;
	}

	public void setPolskeZnaczenie(List<String> polskeZnaczenie) {
		this.polskeZnaczenieList = polskeZnaczenie;
	}

}
