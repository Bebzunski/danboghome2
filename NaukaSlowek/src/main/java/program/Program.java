package program;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

public class Program {

	MyFrame okno;
	FileReader slowka = new FileReader("slowka.txt");
	MyComparator comparator;
	int currentRow;
	private boolean isClickedOKButton = false;
	private boolean isRandom = false;

	public Program() {
		okno = new MyFrame();
		okno.getStartButton().addActionListener(new StartButtonAL());
		okno.getDalej().addActionListener(new NextWordAL());
		okno.getSprawdzButton().addActionListener(new OkButtonAL());
		okno.getSprawdzButton().addKeyListener(new OkButtonKL());
		okno.getDalej().addKeyListener(new NextButtonKL());
		okno.getInfinitiveText().addKeyListener(new InfinitiveTextKL());
		okno.getPastTenseText().addKeyListener(new PastTenseTextKL());
		okno.getPastParticipleText().addKeyListener(new PastParticipleTextKL());
		okno.getStartButton().addKeyListener(new StartButtonKL());
		okno.getOptionButton().addActionListener(new OptionButtonAL());
	}

	public void start() {
		okno.start();
	}

	public void goToMainLayout() {
		comparator = new MyComparator();
		currentRow = -1;
		isClickedOKButton = false;
		okno.setMainLayout();
		isRandom = okno.getRandomCheckbox().getState();
		slowka.load(okno.getVocabularyRange().getSelectedIndex(), isRandom);
		clearText();
		setNextWord();
		setDisableNextButton();
		okno.getInfinitiveText().requestFocus();
	}

	public void goToComparingAnswersStage() {
		if (isClickedOKButton == false) {
			setEnglishWords();
			setResultsColors();
			setDisableText();
			setEnableNextButton();
			setScore();
			isClickedOKButton = true;
		}
		okno.getDalej().requestFocus();
	}

	public void goToNextWordStage() {
		clearText();
		clearAnswers();
		setNextWord();
		setEnableText();
		setEnableOKButton();
		setDisableNextButton();
		resetScoreIfLastWord();
		isClickedOKButton = false;
		okno.getInfinitiveText().requestFocus();
	}

	public void setNextWord() {
		setNextRow();
		okno.setPusty(slowka.getPolskeZnaczenie().get(currentRow));
	}

	public void setNextRow() {
		if (currentRow < slowka.getAmount() - 1) {
			currentRow++;
		} else {
			currentRow = 0;
		}
	}

	public void clearText() {
		okno.clearText();
	}

	public void clearAnswers() {
		okno.getOdpowiedzInfinitive().setText(" ");
		okno.getOdpowiedzPastTense().setText(" ");
		okno.getOdpowiedzPastParticiple().setText(" ");
	}

	public void setEnglishWords() {
		okno.getOdpowiedzInfinitive().setText(slowka.getInfinitiveList().get(currentRow));
		okno.getOdpowiedzPastTense().setText(slowka.getPastTenseList().get(currentRow));
		okno.getOdpowiedzPastParticiple().setText(slowka.getPastParticipleList().get(currentRow));
	}

	public void setDisableText() {
		okno.setDisableText();
	}

	public void setEnableText() {
		okno.setEnableText();
	}

	@SuppressWarnings("deprecation")
	public void setDisableOKButton() {
		okno.getSprawdzButton().disable();
	}

	@SuppressWarnings("deprecation")
	public void setEnableOKButton() {
		okno.getSprawdzButton().enable();
	}

	@SuppressWarnings("deprecation")
	public void setDisableNextButton() {
		okno.getDalej().disable();
	}

	@SuppressWarnings("deprecation")
	public void setEnableNextButton() {
		okno.getDalej().enable();
	}

	public void setResultsColors() {
		okno.setCorrectAnswersColors(comparator.compare(getAllTexts(), getResults()));
	}

	public List<String> getResults() {
		List<String> results = new ArrayList<String>();
		results.add(slowka.getInfinitiveList().get(currentRow));
		results.add(slowka.getPastTenseList().get(currentRow));
		results.add(slowka.getPastParticipleList().get(currentRow));
		return results;
	}

	public List<String> getAllTexts() {
		List<String> texts = new ArrayList<String>();
		texts.add(okno.getInfinitiveText().getText());
		texts.add(okno.getPastTenseText().getText());
		texts.add(okno.getPastParticipleText().getText());
		return texts;
	}

	public void setScore() {
		okno.getScore().setText("       WYNIK : " + comparator.getGoodAnswers() + " / " + comparator.getSample());
	}

	public void resetScoreIfLastWord() {
		if (comparator.getSample() == slowka.getAmount()) {
			comparator.setGoodAnswers(0);
			comparator.setSample(0);
			okno.getScore().setText("       WYNIK : " + comparator.getGoodAnswers() + " / " + comparator.getSample());
		}
	}

	///////////////////////////////////////////////////////////////////////

	public class InfinitiveTextKL implements KeyListener {

		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				okno.getPastTenseText().requestFocus();
			}
		}

		public void keyReleased(KeyEvent e) {

		}

		public void keyTyped(KeyEvent e) {

		}
	}

	public class PastTenseTextKL implements KeyListener {

		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				okno.getPastParticipleText().requestFocus();
			}
		}

		public void keyReleased(KeyEvent e) {

		}

		public void keyTyped(KeyEvent e) {

		}
	}

	public class PastParticipleTextKL implements KeyListener {

		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				okno.getSprawdzButton().requestFocus();
			}
		}

		public void keyReleased(KeyEvent e) {

		}

		public void keyTyped(KeyEvent e) {

		}
	}

	public class StartButtonKL implements KeyListener {

		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				goToMainLayout();
			}
		}

		public void keyReleased(KeyEvent e) {

		}

		public void keyTyped(KeyEvent e) {

		}
	}

	public class StartButtonAL implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			goToMainLayout();
		}
	}

	public class NextWordAL implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			goToNextWordStage();
		}
	}

	public class OkButtonAL implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			goToComparingAnswersStage();
		}
	}

	public class OkButtonKL implements KeyListener {

		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				goToComparingAnswersStage();
			}
		}

		public void keyReleased(KeyEvent e) {

		}

		public void keyTyped(KeyEvent e) {

		}
	}

	public class NextButtonKL implements KeyListener {

		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				goToNextWordStage();
			}
		}

		public void keyReleased(KeyEvent e) {

		}

		public void keyTyped(KeyEvent e) {

		}
	}

	public class OptionButtonAL implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			okno.setOptionPanel();
		}
	}
}
