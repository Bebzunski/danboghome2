package program;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Point;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MyFrame extends Frame {

	private static final long serialVersionUID = 1L;
	
	private int highOfFrame = 400;
	private int lengthOfFrame = 1000;
	
	private Dimension mainDimension = new Dimension(lengthOfFrame, highOfFrame);
	private Dimension optionDimension = new Dimension(lengthOfFrame/2, highOfFrame);
	
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private Point optionPosition = new Point((int) (screenSize.getWidth() - optionDimension.getWidth()) / 2,
			(int) (screenSize.getHeight() - optionDimension.getHeight()) / 2);
	private Point mainPosition = new Point();

	
	private GridLayout mainLayout = new GridLayout(1, 3, 50, 0);

	private GridLayout subLayout = new GridLayout(5, 1, 20, 50);

	private Panel labelPanel = new Panel();
	private Panel textPanel = new Panel();
	private Panel correctAnswersPanel = new Panel();
	private Panel optionPanel = new Panel();

	private Label polskieZnaczenie = new Label(" POLSKIE ZNACZENIE :");
	private Label infinitive = new Label(" INFINITIVE :");
	private Label pastTense = new Label(" PAST TENSE :");
	private Label pastParticiple = new Label(" PAST PARTICIPLE :");
	private Label pusty = new Label();
	private Button optionButton = new Button("OPCJE");
	private Button sprawdzButton = new Button("OK");
	private Button dalej = new Button("NASTĘPNE");
	private TextField infinitiveText = new TextField();
	private TextField pastTenseText = new TextField();
	private TextField pastParticipleText = new TextField();

	private Font myFont = new Font("TimesRoman", Font.PLAIN, 22);
	private Font optionFont = new Font("TimesRoman", Font.BOLD, 30);

	private Color kolorTla = new Color(180, 230, 180);
	private Color kolorSlowa = Color.BLUE;
	private Color badColor = Color.RED;
	private Color goodColor = new Color(0, 180, 0);

	private Label odpowiedzHeader = new Label("ODPOWIEDZI :");
	private Label odpowiedzInfinitive = new Label();
	private Label odpowiedzPastTense = new Label();
	private Label odpowiedzPastParticiple = new Label();
	private Label score = new Label();

	private Label optionHeader = new Label("OPCJE", Label.CENTER);
	private Choice vocabularyRange = new Choice();
	private Checkbox randomCheckbox = new Checkbox(" losowo", false);
	private Button startButton = new Button("START");

	public MyFrame() {

		setTitle("Nauka angielskich czasowników nieregularnych");
		addWindowListener(new WL());
		setBackground(kolorTla);
		setFont(myFont);
		setOptionPanel();
		add(optionPanel);
	}

	public void start() {
		setLocation(optionPosition);
		setResizable(false);
		setVisible(true);
	}

	public void setMainLayout() {

		removeAll();

		mainPosition.setLocation(getLocation().x-250, getLocation().y);
		if(getLocation().x-250 < 0){
			mainPosition.setLocation(1, getLocation().y);
		}
		if((getLocation().x-250)>(screenSize.getWidth() - lengthOfFrame)){
			mainPosition.setLocation(screenSize.getWidth() - lengthOfFrame - 1, getLocation().y);
		}
		
		setLocation(mainPosition);
		setLayout(mainLayout);
		setSize(mainDimension);
		setLabelPanel();
		add(labelPanel);

		setTextPanel();
		add(textPanel);

		setCorrectAnswersPanel();
		add(correctAnswersPanel);
		setEnableText();

	}

	public void setLabelPanel() {
		labelPanel.setLayout(subLayout);
		labelPanel.add(polskieZnaczenie);
		labelPanel.add(infinitive);
		labelPanel.add(pastTense);
		labelPanel.add(pastParticiple);
		labelPanel.add(optionButton);
	}

	public void setTextPanel() {
		textPanel.setLayout(subLayout);
		pusty.setForeground(kolorSlowa);
		textPanel.add(pusty);
		textPanel.add(infinitiveText);
		textPanel.add(pastTenseText);
		textPanel.add(pastParticipleText);
		textPanel.add(sprawdzButton);
	}

	public void setCorrectAnswersPanel() {
		correctAnswersPanel.setLayout(subLayout);
		score.setText(" ");
		correctAnswersPanel.add(score);
		clearCorrectAnswers();
		correctAnswersPanel.add(odpowiedzInfinitive);
		correctAnswersPanel.add(odpowiedzPastTense);
		correctAnswersPanel.add(odpowiedzPastParticiple);
		correctAnswersPanel.add(dalej);
	}

	public void setOptionPanel() {
		removeAll();
		setLayout(null);
		setSize(optionDimension);
		optionHeader.setBounds(100, 50, 100, 50);
		optionHeader.setFont(optionFont);
		add(optionHeader);
		setVocabularyRanges();
		vocabularyRange.setBounds(60, 150, 180, 50);
		add(vocabularyRange);
		randomCheckbox.setBounds(100, 200, 100, 50);
		add(randomCheckbox);
		startButton.setBounds(100, 300, 100, 50);
		add(startButton);
		startButton.requestFocus();
	}

	public void setVocabularyRanges() {
		vocabularyRange.removeAll();
		vocabularyRange.add(" czasowniki na A - B ");
		vocabularyRange.add(" czasowniki na C - E ");
		vocabularyRange.add(" czasowniki na F - H ");
		vocabularyRange.add(" czasowniki na I - R ");
		vocabularyRange.add(" czasowniki na S ");
		vocabularyRange.add(" czasowniki na T - Z ");
		vocabularyRange.add(" wszystkie czasowniki ");
	}

	public void clearText() {
		String empty = " ";
		getInfinitiveText().setText(empty);
		getPastTenseText().setText(empty);
		getPastParticipleText().setText(empty);
	}

	public void clearCorrectAnswers() {
		String empty = " ";
		odpowiedzInfinitive.setText(empty);
		odpowiedzPastTense.setText(empty);
		odpowiedzPastParticiple.setText(empty);
	}

	@SuppressWarnings("deprecation")
	public void setDisableText() {
		infinitiveText.disable();
		pastTenseText.disable();
		pastParticipleText.disable();
	}

	@SuppressWarnings("deprecation")
	public void setEnableText() {
		infinitiveText.enable();
		pastTenseText.enable();
		pastParticipleText.enable();
	}

	public void setCorrectAnswersColors(boolean[] results) {
		if (results.length == 3) {
			setComponentColorDependingOnBoolean(odpowiedzInfinitive, results[0], goodColor, badColor);
			setComponentColorDependingOnBoolean(odpowiedzPastTense, results[1], goodColor, badColor);
			setComponentColorDependingOnBoolean(odpowiedzPastParticiple, results[2], goodColor, badColor);
		}
	}

	private void setComponentColorDependingOnBoolean(Component comp, boolean bool, Color colorIfTrue,
			Color colorIfFalse) {
		comp.setForeground(colorIfFalse);
		if (bool) {
			comp.setForeground(colorIfTrue);
		}
	}

	private static class WL extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			System.exit(0);
		}
	}

	public void setPusty(String slowo) {
		pusty.setText(slowo);
	}

	public Button getDalej() {
		return dalej;
	}

	public void setDalej(Button dalej) {
		this.dalej = dalej;
	}

	public TextField getInfinitiveText() {
		return infinitiveText;
	}

	public void setInfinitiveText(TextField infinitiveText) {
		this.infinitiveText = infinitiveText;
	}

	public TextField getPastTenseText() {
		return pastTenseText;
	}

	public void setPastTenseText(TextField pastTenseText) {
		this.pastTenseText = pastTenseText;
	}

	public TextField getPastParticipleText() {
		return pastParticipleText;
	}

	public void setPastParticipleText(TextField pastParticipleText) {
		this.pastParticipleText = pastParticipleText;
	}

	public Button getSprawdzButton() {
		return sprawdzButton;
	}

	public void setSprawdzButton(Button sprawdzButton) {
		this.sprawdzButton = sprawdzButton;
	}

	public Label getOdpowiedzInfinitive() {
		return odpowiedzInfinitive;
	}

	public void setOdpowiedzInfinitive(Label odpowiedzInfinitive) {
		this.odpowiedzInfinitive = odpowiedzInfinitive;
	}

	public Label getOdpowiedzPastTense() {
		return odpowiedzPastTense;
	}

	public void setOdpowiedzPastTense(Label odpowiedzPastTense) {
		this.odpowiedzPastTense = odpowiedzPastTense;
	}

	public Label getOdpowiedzPastParticiple() {
		return odpowiedzPastParticiple;
	}

	public void setOdpowiedzPastParticiple(Label odpowiedzPastParticiple) {
		this.odpowiedzPastParticiple = odpowiedzPastParticiple;
	}

	public Label getScore() {
		return score;
	}

	public void setScore(Label score) {
		this.score = score;
	}

	public GridLayout getMyLayout() {
		return mainLayout;
	}

	public void setMyLayout(GridLayout myLayout) {
		this.mainLayout = myLayout;
	}

	public Button getStartButton() {
		return startButton;
	}

	public void setStartButton(Button startButton) {
		this.startButton = startButton;
	}

	public Choice getVocabularyRange() {
		return vocabularyRange;
	}

	public void setVocabularyRange(Choice vocabularyRange) {
		this.vocabularyRange = vocabularyRange;
	}

	public Checkbox getRandomCheckbox() {
		return randomCheckbox;
	}

	public void setRandomCheckbox(Checkbox randomCheckbox) {
		this.randomCheckbox = randomCheckbox;
	}

	public Button getOptionButton() {
		return optionButton;
	}

	public void setOptionButton(Button optionButton) {
		this.optionButton = optionButton;
	}
}
