package Zadania_1_8;

public class QuadraticEquation {
	
	private double a;
	private double b;
	private double c;
	
	public QuadraticEquation(double a, double b, double c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public static void main(String[] args) {
		QuadraticEquation qe = new QuadraticEquation(2, 4, -6);
		qe.calcDelta();
		System.out.println(qe.calcDelta());
		System.out.println(qe.giveSolution()[0]+" "+qe.giveSolution()[1]);
	}

	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}
	
	public double calcDelta(){
		return b*b-4*a*c;
	}
	
	public double[] giveSolution(){
		double[] solution;
		if(this.calcDelta() > 0){
			double x1 = (-b-Math.sqrt(calcDelta()))/(2*a);
			double x2 = (-b+Math.sqrt(calcDelta()))/(2*a);
			solution = new double[]{x1,x2};
		} else if(this.calcDelta() == 0){
			double x0 = (-b)/(2*a);
			solution = new double[]{x0};
		} else {
			solution = new double[1];
		}
		return solution;
	}
	
	
}
