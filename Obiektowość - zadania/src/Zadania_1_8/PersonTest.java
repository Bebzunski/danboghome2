package Zadania_1_8;

public class PersonTest {
	
	public static void main(String[] args) {
		
		Person p1 = new Person("Daniel","Boganski",23);
		Person p2 = new Person("Kamila","Boganski",23);
		Person p3 = new Person("Jacek","Boganski",23);
		Person p4 = new Person("Adam","Boganski",23);
	
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		System.out.println(p4);
		
	}

}
