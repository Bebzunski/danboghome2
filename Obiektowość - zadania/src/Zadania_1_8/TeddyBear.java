package Zadania_1_8;

public class TeddyBear {
	
	private String name;

	public TeddyBear(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

}
