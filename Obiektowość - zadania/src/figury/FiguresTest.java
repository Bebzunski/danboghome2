package figury;

public class FiguresTest {

	public static void main(String[] args) {
		
		Figures c1 = new Circle(3);
		Figures s1 = new Square(5);
		Figures t1 = new Triangle(3, 5, 4);
		
		c1.showSentence();
		System.out.println(t1.countArea());
		System.out.println(s1.countArea());

	}

}
