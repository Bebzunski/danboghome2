package figury;

public class Circle implements Figures{
	
	private double r;

	public Circle(double r) {
		super();
		this.r = r;
	}

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}
	
	public double countArea(){
		return Math.PI * r * r;
	}
	
	public double countCircumference(){
		return 2*Math.PI * r;
	}
	
	public void showSentence() {
		System.out.println("Jest to ko�o.");
	}
}
