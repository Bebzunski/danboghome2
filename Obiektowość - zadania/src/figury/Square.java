package figury;

public class Square implements Figures{
	
	private double a;

	public Square(double a) {
		super();
		this.a = a;
	}

	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}
	
	public double countArea(){
		return a*a;
	}
	
	public double countCircumference(){
		return 4*a;
	}
}
