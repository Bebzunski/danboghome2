package figury;

public interface Figures {

	public double countArea();

	public double countCircumference();

	public default void showSentence() {
		System.out.println("Jest to figura p�aska.");
	}

}
