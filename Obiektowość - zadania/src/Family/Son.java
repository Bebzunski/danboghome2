package Family;

public class Son extends FamilyMember{
	
	private String name;

	public Son(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void introduce(){
		System.out.println("I'm son, my name is "+this.getName());
	}
}
