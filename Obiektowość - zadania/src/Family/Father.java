package Family;

public class Father extends FamilyMember{
	
	private String name;

	public Father(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public  void introduce(){
		System.out.println("I'm father, my name is "+this.getName());
	}
	
}
