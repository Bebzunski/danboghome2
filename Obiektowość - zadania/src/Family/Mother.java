package Family;

public class Mother extends FamilyMember{

	private String name;

	public Mother(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public  void introduce(){
		System.out.println("I'm mother, my name is "+name);
	}
}
