package zadaniaDodatkowe;

public interface DataProvider {
	
	public int nextInt(String name);
	
	public String nextString(String name);

}
