package myCollection;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import myset.MySet;

public class MySetTest {
	
	private MySet set;
	
	@Before
	private void init(){
		set = new MySet();
	}

	@Test
	public void addTest1() {
		
		int[] expecteds = {1,-6,2,0};
		set.add(1);
		set.add(-6);
		set.add(1);
		set.add(2);
		set.add(0);
		set.add(-6);
		set.add(0);
		set.add(1);
		
		assertArrayEquals(expecteds, set.getData());
		assertEquals(expecteds.length, set.getNumber());
	}
	
	@Test
	public void addTest2() {
		
		int[] expecteds = {};
		
		assertArrayEquals(expecteds, set.getData());
		assertEquals(expecteds.length, set.getNumber());
	}

}
