package myset;

public class MySet {

	private int number = 0;
	private int[] data = new int[100];

	public void add(int num) {

		for (int i = 0; i < number; i++) {
			if (data[i] == num) {
				break;
			} else {
				data[number] = num;
				number++;
			}
		}

	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int[] getData() {
		return data;
	}

	public void setData(int[] data) {
		this.data = data;
	}

}
