package sklep;

import java.util.ArrayList;
import java.util.List;

public class Store {

	private List<Product> products = new ArrayList<>();

	public List<Product> getProducts() {
		return products;
	}

	public void add(Product product) {
		products.add(product);
	}

	public void showProduct(Category category) {
		for (int i = 0; i < products.size(); i++) {
			if (products.get(i).getCategory() == category) {
				System.out.println(products.get(i));
			}
		}
	}
}
