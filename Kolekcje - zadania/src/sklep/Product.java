package sklep;

public class Product {
	
	private String name;
	private int cena;
	private Category category;
	
	public Product(String name, int cena, Category category) {
		super();
		this.name = name;
		this.cena = cena;
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCena() {
		return cena;
	}

	public void setCena(int cena) {
		this.cena = cena;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return name+" : "+ cena+"z�";
	}
	
	

}
