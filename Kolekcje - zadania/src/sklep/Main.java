package sklep;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Store sklep = new Store();
		sklep.add(new Product("buty", 55, Category.CLOTHES));
		sklep.add(new Product("w�dka", 39, Category.ALCOHOL));
		sklep.add(new Product("TV", 550, Category.ELECTRONICS));
		sklep.add(new Product("pierogi", 9, Category.FOOD));
		sklep.add(new Product("koszula", 24, Category.CLOTHES));
		sklep.add(new Product("piwo", 3, Category.ALCOHOL));
		sklep.add(new Product("telefon", 200, Category.ELECTRONICS));
		sklep.add(new Product("chleb", 2, Category.FOOD));

		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj interesuj�cy Ciebie produkt.");
		String produkt = sc.next();

		for (Product element : sklep.getProducts()) {
			if (element.getCategory() == getCategory(sklep, produkt)) {
				System.out.println(element);
			}
		}
		sc.close();
	}

	public static Category getCategory(Store sklep, String produkt) {

		for (Product element : sklep.getProducts()) {
			if (element.getName().equalsIgnoreCase(produkt)) {
				return element.getCategory();
			}
		}

		return Category.UNNONE;
	}

}
