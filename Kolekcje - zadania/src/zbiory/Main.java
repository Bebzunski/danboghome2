package zbiory;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {

	public static void main(String[] args) {

		int[] arr = { 100, 200, 100, 300, 400, 200, 20, 300, 2000, 4000, 500 };
		Set<Integer> zbior = new TreeSet<>();

		for (int el : arr) {
			zbior.add(el);
		}
		System.out.println(zbior.size());
		
		for(Integer el : zbior){
			System.out.print(el+" ");
		}
		System.out.println();
		zbior.remove(100);
		zbior.remove(200);
		
		for(Integer el : zbior){
			System.out.print(el+" ");
		}
		System.out.println();
		
		System.out.println(containDuplicates(" Danield  "));
		
		Set<ParaLiczb> paryLiczb = new HashSet<>();
		paryLiczb.add(new ParaLiczb(1, 2));
		paryLiczb.add(new ParaLiczb(2 , 1));
		paryLiczb.add(new ParaLiczb(1, 1));
		paryLiczb.add(new ParaLiczb(1, 2));
		
		for(ParaLiczb element : paryLiczb){
			System.out.println(element);
		}
		
	}
	
	public static boolean containDuplicates(String word){
		if(word.trim().split(" ").length==1){
		
		Set<String> letters = new HashSet<>();
		for(String letter : word.trim().split("")){
			letters.add(letter.toLowerCase());
		}
		return letters.size()==word.trim().length();
		} else {
			System.out.println("To nie jest jedno s�owo: "+"'"+word.trim()+"' ");
			return false;
		}
	}

}
