package mapy;

//import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Main {
	
	public static void main(String[] args) {
		
		System.exit(0);
		
		Uczelnia u = new Uczelnia();
		u.dodajStudenta(new Student(114065, "Damian", "Kurczak"));
		u.dodajStudenta(new Student(114064, "Jarek", "Kaczy�ski"));
		u.dodajStudenta(new Student(114066, "R�a", "Iwan"));
		for(Student student : u.getMapOfStudents().values()){
			System.out.println(student);
		}
		System.out.println(u.czyIstniejeStudent(114064));
		System.out.println(u.pobierzStudenta(114065));
		System.out.println(u.liczbaStudentow());
		u.wypiszWszystkich();
		
		
		Map<Integer, Student> map = new TreeMap<>();
		map.put(100200, new Student(100200,"Daniel","Bogdalski"));
		map.put(100600, new Student(100600,"Anna","Kowlaska"));
		map.put(100400, new Student(100400,"Marian","Pa�dzioch"));
		System.out.println(map.containsKey(100200));
		System.out.println(map.get(100600));
		System.out.println(map.size());
		for(Student student : map.values()){
			System.out.println(student);
		}
	}

}
