package mapy;

import java.util.Map;
import java.util.TreeMap;

public class Uczelnia {

	private Map<Integer, Student> mapOfStudents = new TreeMap<>();

	public Map<Integer, Student> getMapOfStudents() {
		return mapOfStudents;
	}

	public void dodajStudenta(Student student) {

		mapOfStudents.put(student.getNumerIndeksu(), student);

	}

	public boolean czyIstniejeStudent(Integer numerIndeksu) {
		for (Student student : mapOfStudents.values()) {
			if (student.getNumerIndeksu() == numerIndeksu) {
				return true;
			}
		}
		return false;
	}
	
	public Student pobierzStudenta(Integer numerIndeksu){
		return mapOfStudents.get(numerIndeksu);
	}
	
	public int liczbaStudentow(){
		return mapOfStudents.size();
	}
	
	public void wypiszWszystkich(){
		for(Student student : mapOfStudents.values()){
			System.out.println(student.getImie()+" "+student.getNaziwsko());
		}
	}
}
