package mapy;

public class Student {
	
	private int numerIndeksu;
	private String imie;
	private String naziwsko;
	
	public Student(int numerIndeksu, String imie, String naziwsko) {
		super();
		this.numerIndeksu = numerIndeksu;
		this.imie = imie;
		this.naziwsko = naziwsko;
	}
	public int getNumerIndeksu() {
		return numerIndeksu;
	}
	public String getImie() {
		return imie;
	}
	public String getNaziwsko() {
		return naziwsko;
	}
	@Override
	public String toString() {
		return "Student [numerIndeksu=" + numerIndeksu + ", imie=" + imie + ", naziwsko=" + naziwsko + "]";
	}
	
	

}
