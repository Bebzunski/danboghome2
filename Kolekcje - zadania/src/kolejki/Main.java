package kolejki;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class Main {

	public static void main(String[] args) {
		
		Queue<Person> q2 = new PriorityQueue<>(new Comparator<Person>() {
			
			@Override
			public int compare(Person p1, Person p2) {
				return p1.getName().compareTo(p2.getName());
			}
		});

		q2.add(new Person("Adam", 35));
		q2.add(new Person("Daniel", 33));
		q2.add(new Person("Ada", 45));
		q2.add(new Person("Iwan", 34));

		while (!q2.isEmpty()) {
			System.out.println(q2.poll());
		}

		System.exit(0);

		Queue<Integer> q1 = new PriorityQueue<>();
		q1.add(1);
		q1.add(21);
		q1.add(2);
		q1.add(10);
		q1.add(12);

		for (Integer element : q1) {
			System.out.print(element + " ");
		}

		q1.poll();
		System.out.println();
		for (Integer element : q1) {
			System.out.print(element + " ");
		}
	}

}
