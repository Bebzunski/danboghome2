package listy;

import java.util.ArrayList;
import java.util.List;

public class Main {

	int a;

	public static void main(String[] args) {

		User u1 = new User("Bodzioadd", "madafaka");
		User u2 = new User("Bodo", "madafakdfgdfa");
		User u3 = new User("afaf", "madaasdfdfgsfdgdfgfaka");
		List<User> list2 = new ArrayList<>();
		list2.add(u1);
		list2.add(u2);
		list2.add(u3);
		list2.add(new User("Natka", "razdwatrzy"));

		list2.remove(2);
		System.out.println(list2.get(2));
		System.out.println();

		for (User el : list2) {
			System.out.println(User.toString(el));
		}

		List<Integer> list1 = new ArrayList<>();
		list1.add(4);
		list1.add(7);
		list1.add(9);
		list1.add(-5);
		System.out.println(avgElements(list1));

		int[] array = { 1, 7, 8, -5, 8, 4 };

		System.out.println(compareNumbers(array, list1));

		List<User> list3 = copyList(list2);
		list3.clear();
		
		double[] arrayofUsers = fromListToArray(list1);
		arrayofUsers[2] = 99;

	}

	private static double avgElements(List<Integer> list1) {

		double sum = 0;
		for (int element : list1) {
			sum += element;
		}
		return sum / list1.size();
	}

	public static int compareNumbers(int[] array, List<Integer> list) {

		int length;
		if (array.length <= list.size()) {
			length = array.length;
		} else {
			length = list.size();
		}
		int count = 0;
		for (int i = 0; i < length; i++) {
			if (array[i] == list.get(i)) {
				count++;
			}
		}
		return count;
	}

	public static <T> List<T> copyList(List<T> list) {

		List<T> copyOfList = new ArrayList<>(list.size());
		for (int i = 0; i < list.size(); i++) {
			copyOfList.add(list.get(i));
		}
		return copyOfList;
	}

	public static double[] fromListToArray(List<Integer> list) {
		double[] array = new double[list.size()];
		for (int i = 0; i < list.size(); i++) {
			array[i] = list.get(i);
		}
		return array;
	}

}
