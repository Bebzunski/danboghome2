package listy;

public class User {
	
	private String name;
	private String password;
	
	public User(String name, String password) {
		super();
		this.name = name;
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	
	public static String toString(User user) {
		return "Name : "+user.name+" , password : "+user.password+" .";
	}
	
	@Override
	public String toString() {
		return "Name : "+this.name+" , password : "+this.password;
	}
	
	
}
