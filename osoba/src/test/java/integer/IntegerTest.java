package integer;

import org.junit.Test;

public class IntegerTest {

	@Test
	public void test(){
		
		Integer a = 10;
		Integer b = a;
		a = 5;
		assert a == 5;
		assert b == 10;
		
		a++;
		assert a == 6;
		assert b == 10;
		
	}

}
