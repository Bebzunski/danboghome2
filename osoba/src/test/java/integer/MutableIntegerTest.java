package integer;

import org.junit.Test;

public class MutableIntegerTest {
	
	@Test
	public void test(){
		
		MutableInteger a = new MutableInteger(10);
		MutableInteger b = a;
		
		assert a.getValue() == 10;
		assert b.getValue() == 10;
		
		a.setValue(5);
		
		assert a.getValue() == 5;
		assert b.getValue() == 5;
		
		a.increment();
		assert a.getValue() == 6;
		assert b.getValue() == 6;
		
		String c = "daniel";
		String d = new String("daniel");
		String f = c;
		
		assert c==f;
		assert c.equals(d);
	}

}
