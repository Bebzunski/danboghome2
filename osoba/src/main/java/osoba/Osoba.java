package osoba;

public class Osoba {

	private String imie;
	private boolean czyKobieta;
	private int wiek;
	private double masa;
	private double wzrost;
	
	public Osoba(String imie, boolean k,int wiek, double masa,double wzrost) {
		super();
		this.masa = masa;
		this.wzrost = wzrost;
		this.imie = imie;
		this.czyKobieta = k;
		this.wiek = wiek;
		
	}
	
	public Osoba(double masa,double wzrost) {
		super();
		this.masa = masa;
		this.wzrost = wzrost;
		
	}

	public double countBMI() {
		return masa/Math.pow(wzrost,2);
	}

	public void ktoTo(){
		System.out.println(imie+" lat "+wiek+" waga "+masa+" czy kobieta : "+czyKobieta);
	}
}
