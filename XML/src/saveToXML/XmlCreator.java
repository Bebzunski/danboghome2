package saveToXML;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlCreator {

	public static void main(String[] args) {
		
		Student s1 = new Student("Daniel","Bogdalski",3);
		Student s2 = new Student("Krzysztof","Kura�",1);

		try {
			Document doc = newDocument();

			Element root = doc.createElement("ROOT");
			doc.appendChild(root);
			Element students = doc.createElement("STUDENTS");
			root.appendChild(students);
			students.setAttribute("kierunek", "Matematyka Stosowana");
			addStudent(doc, students, s1);
			addStudent(doc, students, s2);
			
			saveFile(doc, "resources/students.xml");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Document newDocument() throws ParserConfigurationException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.newDocument();
		return doc;
	}

	public static void saveFile(Document doc, String fileName)
			throws TransformerFactoryConfigurationError, TransformerConfigurationException, TransformerException {
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(fileName));
		transformer.transform(source, result);
		// Output to console for testing
		StreamResult consoleResult = new StreamResult(System.out);
		transformer.transform(source, consoleResult);
	}
	
	private static void addStudent(Document doc, Element students, Student student){
		
		Element studentElement = doc.createElement("STUDENT");
		students.appendChild(studentElement);
		
		Element name = doc.createElement("NAME");
		studentElement.appendChild(name);
		name.appendChild(doc.createTextNode(student.getName()));
		
		Element lastname = doc.createElement("LASTNAME");
		studentElement.appendChild(lastname);
		lastname.appendChild(doc.createTextNode(student.getLastname()));
		
		Element year = doc.createElement("YEAR");
		studentElement.appendChild(year);
		year.appendChild(doc.createTextNode(student.getYear()+""));
	}

}
