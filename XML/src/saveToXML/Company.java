package saveToXML;

public class Company {
	
	private String name;
	private int startingYear;
	private int employees;
	private int vat;
	
	public Company(String name, int startingYear, int employees, int vat) {
		super();
		this.name = name;
		this.startingYear = startingYear;
		this.employees = employees;
		this.vat = vat;
	}

	public String getName() {
		return name;
	}

	public int getStartingYear() {
		return startingYear;
	}

	public int getEmployees() {
		return employees;
	}

	public int getVat() {
		return vat;
	}
	
	
}
