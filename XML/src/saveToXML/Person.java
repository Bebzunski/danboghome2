package saveToXML;

public class Person {
	
	private String name;
	private String lastname;
	private String company;
	private double salary;
	private String department;
	private int yearOfBorn;
	
	public Person(String name, String lastname, String company, double salary, String department, int yearOfBorn) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.company = company;
		this.salary = salary;
		this.department = department;
		this.yearOfBorn = yearOfBorn;
	}

	public String getName() {
		return name;
	}

	public String getLastname() {
		return lastname;
	}

	public String getCompany() {
		return company;
	}

	public double getSalary() {
		return salary;
	}

	public String getDepartment() {
		return department;
	}

	public int getYearOfBorn() {
		return yearOfBorn;
	}
	
	
}
