package saveToXML;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class People {

	private List<Person> people = new ArrayList<>();

	public List<Person> getPeople() {
		return people;
	}

	public void addPerson(Person person) {
		people.add(person);
	}

	public void savePeople() {

		try {
			Document doc = XmlCreator.newDocument();
			Element root = doc.createElement("People");
			doc.appendChild(root);
			for (Person person : people) {
				addPerson(doc, root, people, person);
			}

			XmlCreator.saveFile(doc, "resources/people.xml");

		} catch (ParserConfigurationException | TransformerFactoryConfigurationError | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void addPerson(Document doc, Element root, List<Person> people , Person person) {
		Element personElement = doc.createElement("Person");
		root.appendChild(personElement);
		
		Element name = doc.createElement("Name");
		personElement.appendChild(name);
		name.appendChild(doc.createTextNode(person.getName()));
		
		Element lastname = doc.createElement("Lastname");
		personElement.appendChild(lastname);
		lastname.appendChild(doc.createTextNode(person.getLastname()));
		
		Element company = doc.createElement("Company");
		personElement.appendChild(company);
		company.appendChild(doc.createTextNode(person.getCompany()));
		
		Element salary = doc.createElement("Salary");
		personElement.appendChild(salary);
		salary.appendChild(doc.createTextNode(person.getSalary()+""));
		
		Element department = doc.createElement("Salary");
		personElement.appendChild(department);
		department.appendChild(doc.createTextNode(person.getDepartment()));
		
		Element yearOfBorn = doc.createElement("Born");
		personElement.appendChild(yearOfBorn);
		yearOfBorn.appendChild(doc.createTextNode(person.getYearOfBorn()+""));
		
	}
	

}
