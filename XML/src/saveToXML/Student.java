package saveToXML;

public class Student {
	
	private String name;
	private String lastname;
	private int year;
	
	public Student(String name, String lastname, int year) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.year = year;
	}

	public String getName() {
		return name;
	}

	public String getLastname() {
		return lastname;
	}

	public int getYear() {
		return year;
	}
	
}
