package saveToXML;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CompanyXML {

	public static void main(String[] args) {

		Company c1 = new Company("Testowa", 1987, 234, 21);
		Company c2 = new Company("Testowa 2", 1993, 110, 8);
		Company c3 = new Company("Testowa 3", 1999, 5000, 40);

		try {
			Document doc = XmlCreator.newDocument();
			Element root = doc.createElement("ROOT");
			doc.appendChild(root);
			
			addCompany(doc, root, c1);
			addCompany(doc, root, c2);
			addCompany(doc, root, c3);

			XmlCreator.saveFile(doc, "resources/companies.xml");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void addCompany(Document doc, Element root, Company company) {
		Element companyElement = doc.createElement("COMPANY");
		companyElement.setAttribute("name", company.getName());
		root.appendChild(companyElement);
		
		Element starts = doc.createElement("STARTS");
		companyElement.appendChild(starts);
		starts.appendChild(doc.createTextNode(company.getStartingYear()+""));
		
		Element employees = doc.createElement("EMPLOYEES");
		companyElement.appendChild(employees);
		employees.appendChild(doc.createTextNode(company.getEmployees()+""));
		
		Element vat = doc.createElement("VAT");
		companyElement.appendChild(vat);
		vat.appendChild(doc.createTextNode(company.getVat()+""));
	}

}
