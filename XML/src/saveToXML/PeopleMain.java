package saveToXML;

public class PeopleMain {

	public static void main(String[] args) {
		
		People people = new People();
		people.addPerson(new Person("Daniel","Bogdalski","Intel",2500,"frontend",1987));
		people.addPerson(new Person("Jurek","Noawk","Adidas",2500,"tech",1983));
		people.savePeople();

	}

}
