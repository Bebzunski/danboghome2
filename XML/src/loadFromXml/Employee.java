package loadFromXml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Employee {

	public static void main(String[] args) {
		

		Employee empl = new Employee();
		System.out.println(empl.getAvgSalary());

		System.out.println();

		for (String sentence : getNames()) {
			System.out.println(sentence);
		}
		
		empl.getMinMaxStaffID();
	}

	public void getMinMaxStaffID() {

		try {
			Document doc = parseFile("resources/staff.xml");
			NodeList employees = doc.getElementsByTagName("staff");
			double[] employeesID = new double[employees.getLength()];
			
			for(int i = 0; i < employees.getLength(); i++){
				Element employee = (Element) employees.item(i);
				employeesID[i]= Double.parseDouble(employee.getAttribute("id"));
			}
			
			System.out.println("Najmniejsze ID: "+minInArray(employeesID));
			System.out.println("najwieksze ID: "+maxInArray(employeesID));

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public double getAvgSalary() {

		try {
			Document doc = parseFile("resources/staff.xml");
			NodeList employees = doc.getElementsByTagName("staff");

			double sum = 0;

			for (int i = 0; i < employees.getLength(); i++) {
				Node employeeNode = employees.item(i);
				Element employee = (Element) employeeNode;
				sum += Double.parseDouble(employee.getElementsByTagName("salary").item(0).getTextContent());
			}

			return sum / employees.getLength();

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

	public static List<String> getNames() {
		List<String> list = new ArrayList<>();

		try {
			Document doc = parseFile("resources/staff.xml");
			NodeList employees = doc.getElementsByTagName("staff");

			for (int i = 0; i < employees.getLength(); i++) {
				Element employee = (Element) employees.item(i);
				list.add(employee.getElementsByTagName("firstname").item(0).getTextContent() + " "
						+ employee.getElementsByTagName("lastname").item(0).getTextContent());
			}

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return list;
	}

	public static Document parseFile(String pathToFile) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new File(pathToFile));
		doc.getDocumentElement().normalize();
		return doc;
	}

	private static double max(double a, double b) {
		if (a >= b) {
			return a;
		} else {
			return b;
		}
	}
	
	private static double min(double a, double b) {
		if (a <= b) {
			return a;
		} else {
			return b;
		}
	}
	
	private static double minInArray(double[] array) {
		double min = array[0];
		for(int i = 1 ; i<array.length ; i++){
			min = min(min,array[i]);
		}
		return min;
	}
	
	private static double maxInArray(double[] array) {
		double min = array[0];
		for(int i = 1 ; i<array.length ; i++){
			min = max(min,array[i]);
		}
		return min;
	}
}
