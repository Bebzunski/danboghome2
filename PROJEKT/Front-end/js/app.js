var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8088/';

app.config(function($routeProvider) {
    var path = './views/';
    $routeProvider
        .when('/', {
            templateUrl: path + 'login.html',
            controller: 'loginController'
        }).when('/main', {
            templateUrl: path +'main.html',
            controller: 'betController'
        });
});

app.service('loginService', function() {
    
    this.layoutPath = "";
    this.cardPath = "";
    
    this.setLayoutPath = function(layout) {
        this.layoutPath = layout;
    }
    
    this.getLayoutPath = function() {
        return this.layoutPath;
    }
    
    this.setCardPath = function(card) {
        this.cardPath = card;
    }
    
    this.getCardPath = function() {
        return this.cardPath;
    }
    
});


app.controller('loginController', function($scope, $http, $window, $location, loginService) {
    
    $scope.name ="";
    $scope.password ="";
    $scope.repeatPass ="";
    $scope.lauoutS = 'blue';
    $scope.cardBackS = "black";
    
    $scope.tryLogin = function() { 
        
        loginService.setLayoutPath('../gfx/layout_'+ $scope.lauoutS +'.jpg');
        loginService.setCardPath('../gfx/cards/back_'+ $scope.cardBackS +'.jpg');

        if($scope.name ==="" || $scope.password ===""){
            alert("WYPEŁNIJ WSZYSTKIE POZYCJE !!");
        } else {
    
        $http({
                url: url + 'users/login',
                dataType: 'json',
                params: {
                    name: $scope.name,
                    password: $scope.password
                }
            }).then(function(success) {
            
                    $scope.bool = success.data;
            
                    if($scope.bool[0]=="true"){
                        alert("Życzę powodzenia "+$scope.name+" !");
                        //$window.location.href = "/#!main";
                        $location.path("/main");
                        
                    } else {
                        alert("NIEPOPRAWNY LOGIN LUB HASŁO !!");
                        $scope.name="";
                        $scope.password="";
                    }
            
                }, function(error) {
                    console.error(error);
            });
        }
    }
    
    $scope.tryRegister = function() {
        
        if($scope.name ==="" || $scope.password==="" || $scope.repeatPass===""){
            alert("WYPEŁNIJ WSZYSTKIE POZYCJE !!");
        }else if($scope.password != $scope.repeatPass){
            alert("HASŁA SIĘ RÓŹNIĄ !!");
        } else {           
            console.log($scope.password, $scope.repeatPass);
            $http({
                url: url + 'users/add',
                dataType: 'json',
                params: {
                    name: $scope.name,
                    password: $scope.password
                }
            }).then(function(success) {
                $scope.user = success.data;
                if($scope.user.id > 0){
                    alert("Udana rejestracja :)");
                    $scope.password="";
                    $scope.repeatPass="";
                    $('#loginButton').css('visibility', 'visible');
                    $('#repeatLabel').css('visibility', 'hidden');
                    $('#repeatInput').css('visibility', 'hidden');
                    $('#registerButton').css('visibility', 'hidden');
                    $('#register')[0].checked = false;
                } else {
                    alert("JUŻ ISTNIEJE KONTO O TAKIEJ NAZWIE !!");
                    $scope.name="";
                    $scope.password="";
                    $scope.repeatPass="";
                }
                
                }, function(error) {
                    console.error(error);
            });
        }
    }
    
    $('#register')[0].checked = false;
    
    $(document).ready(function() {

        $('#register').on('change', function(e) {
            
            if($(this)[0].checked){
                $('#loginButton').css('visibility', 'hidden');
                $('#repeatLabel').css('visibility', 'visible');
                $('#repeatInput').css('visibility', 'visible');
                $('#registerButton').css('visibility', 'visible');
            } else {
                $('#loginButton').css('visibility', 'visible');
                $('#repeatLabel').css('visibility', 'hidden');
                $('#repeatInput').css('visibility', 'hidden');
                $('#registerButton').css('visibility', 'hidden');
            }
        });
        
    });
    
    
       
});


app.controller('betController', function($scope, $http, loginService) {
        
        $('#mainContainer').css('background-image', 'url('+loginService.getLayoutPath()+')');
        
    var startButt = document.getElementById('startButton');
        startButt.disabled = "true";
        startButt.style.backgroundColor = "firebrick";
    var stopButt = document.getElementById('stopButton');
        stopButt.disabled = "true";
        stopButt.style.backgroundColor = "firebrick";
    var changeButt = document.getElementById('changeButton');
        changeButt.style.visibility = "hidden";
    var betButt = document.getElementById('betButton')
        betButt.disabled = "true";
        betButt.style.backgroundColor = "firebrick";
    var variantSel = document.getElementById('variantSelect');
    var variantLab = document.getElementById('variant');
    var cards = document.getElementsByClassName('card');
    var info = document.getElementById('information');
    var result = document.getElementById('result');
    var logout = document.getElementById('logout');
    var toChange = [false, false , false , false , false ];
    
    var variants = [{value:1.1 , rank: 400 , text:"Wysoka karta | A : 1.1"},
                    {value:3.0 , rank: 500 , text:"Para | A : 3.0"},
                    {value:5.0 , rank: 600 , text:"Dwie pary | A na K : 5.0"},
                    {value:15.0 , rank: 700 , text:"Trójka | A : 15.0"},
                    {value:50.0 , rank: 800 , text:"Strit | od A : 50.0 "},
                    {value:250.0 , rank: 900 , text:"Full | A na K : 250.0"},
                    {value:1000.0 , rank: 1000 , text:"Kareta | A: 1000.0"}]; 
    
    function getRankByValue(value) {
        for(let v of variants) {
            if(v.value == value) {
                return v.rank;
            }
        }
        return 0;
    }

    //var info = $('#information').css('visibility', 'hidden');
    
    $scope.actualChips = 1000;
    $scope.betsize = 5;
    $scope.myVariants = variants;
    console.log(loginService.getLayoutPath());
    
    $scope.variantHistory = [];
    $scope.nrOfRound = 1;
    $scope.resultS = "";
    
    $scope.setBet = function() {
        
        $http({
            url: url + 'game/hand',
            dataType: 'json'
        }).then(function(success) {
            $scope.hand = success.data;
        }, function(error) {
            console.error(error);
        });
        
        $scope.actualChips = $scope.actualChips - $scope.betsize;
        $scope.multiplier = Math.round($scope.variantSel * $scope.betsize * 10)/10;
        
        info.style.visibility = "visible";
        changeButt.style.visibility = "false";

        betButt.disabled = "true";
        betButt.style.backgroundColor = "firebrick";

        startButt.style.backgroundColor = "red";
        startButt.removeAttribute("disabled"); 

        stopButt.style.backgroundColor = "red";
        stopButt.removeAttribute("disabled");

        variantSel.disabled = "true";
        betSize.style.visibility = "hidden";
        logout.style.visibility = "hidden";
        
        $scope.visibleCards();
        $scope.backCards();
        
    }
    
    $scope.stop = function() {
        
        $scope.actualChips = $scope.actualChips + $scope.betsize;
        info.style.visibility = "hidden";
        changeButt.style.visibility = "hidden";
        variantLab.style.visibility = "hidden";

        betButt.removeAttribute("disabled");
        betButt.style.backgroundColor = "red";

        startButt.style.backgroundColor = "firebrick";
        startButt.disabled = "true"; 

        stopButt.style.backgroundColor = "firebrick";
        stopButt.disabled = "true";

        variantSel.removeAttribute("disabled");
        betSize.style.visibility = "visible";
        
        logout.style.visibility = "visible";
        
        $scope.invisibleCards();

    }
    
    $scope.start = function() {
        
        $scope.getVariant();
        changeButt.style.visibility = 'visible';
        variantLab.style.visibility = 'visible';
        startButt.disabled = true;
        startButt.style.backgroundColor = "firebrick";
        stopButt.disabled = true;
        stopButt.style.backgroundColor = "firebrick";

        $scope.showCards();
         
    }
    
    $scope.change = function() {
        
        var index = 0;
        for(let card of cards){         
            if($(card).hasClass('mt-30')) {
                toChange[index] = true;  
            } else {
                toChange[index] = false;
            }
            index++;
        }
        
        $scope.toChange = toChange;
        
        $http({
            url: url + 'game/change',
            dataType: 'json',
            params: {
                change: $scope.toChange
            }
        }).then(function(success) {
                $scope.hand = success.data;
                $scope.showCards();
                $scope.getVariant();
                $scope.cardsDown();
            
                $http({
                    url: url + 'game/rank',
                    dataType: 'json',
                }).then(function(success) {
                    $scope.rank = success.data;
                    $scope.winOrLose();
                }, function(error) {
                    console.error(error);
                });
            
        }, function(error) {
            console.error(error);
        });
                     
    }
    
    $scope.getVariant = function() {
        
        $http({
            url: url + 'game/variant',
            dataType: 'json'
        }).then(function(success) {
            $scope.actualVariant = success.data;
        }, function(error) {
            console.error(error);
        });
    }
    
        
    $scope.winOrLose = function() {
        
        if($scope.rank > getRankByValue($scope.variantSel)){
            $scope.actualChips = $scope.actualChips + $scope.multiplier ;
            $scope.result = "WYGRYWASZ !";
            $scope.resultS = "W"
            result.style.backgroundColor= "lime";
        } else {
            $scope.result = "PRZEGRYWASZ"
            result.style.backgroundColor= "tomato";
            $scope.resultS = "L";
        }
        
        $scope.betsize = 5 ;
        if($scope.variantHistory.length>4){
            $scope.variantHistory.shift();
        }
        $scope.variantHistory.push({nr: $scope.nrOfRound, text: $scope.actualVariant[0] , result: $scope.resultS });
        $scope.nrOfRound++;
        $scope.endRound();
        result.style.visibility = "visible";
    }
    
    $scope.startNewRound = function(){
        
        $http({
            url: url + 'game/end',
            dataType: 'json'
        }).then(function(success) {
        }, function(error) {
            console.error(error);
        });
        
        $scope.cardsDefault();
        $scope.invisibleCards();
        result.style.visibility = "hidden";
        variantSel.removeAttribute('disabled');
        betButt.removeAttribute('disabled');
        betButt.style.backgroundColor = "red";
        betSize.style.visibility = "visible";
        info.style.visibility = "hidden";
        variantLab.style.visibility = "hidden";
        
        logout.style.visibility = "visible";
        $scope.nrCardsToChange = 0 ;
    }
        
    $scope.endRound = function(){
        
        $scope.cardsDown();
        changeButt.style.visibility = "hidden";
        info.style.visibility = "hidden";

        stopButt.disabled = "true";
        betButt.style.backgroundColor = "firebrick";

        startButt.style.backgroundColor = "firebrick";
        startButt.disabled = "true";

        stopButt.style.backgroundColor = "firebrick";
        stopButt.disabled = "true"; 

        betSize.style.visibility = "hidden";
    }
    
    $scope.cardsDefault = function(){
        
        $scope.backCards();
        
        for(let card of cards){

            if($(card).hasClass('mt-30')) {
                $(card).removeClass('mt-30');
            }
        }
    }
    
    $scope.cardsDown = function(){
        
        for(let card of cards){
            if($(card).hasClass('mt-30')) {
                $(card).removeClass('mt-30');
            }
        }
    }
    
    $scope.showCards = function(){
        
        var index = 0;
        for(let card of cards){
        card.style.backgroundImage = "url('gfx/cards/"+$scope.hand[index]+".jpg')";
            index++;
        }
    }
    
    $scope.backCards = function(){
        
        for(let card of cards){
        card.style.backgroundImage = 'url('+loginService.getCardPath()+')';
        }
    }
    
    $scope.invisibleCards = function(){
        
        for(let card of cards){
        card.style.visibility = "hidden";
        }
    }
    
     $scope.visibleCards = function(){
        
        for(let card of cards){
        card.style.visibility = "visible";
        }
    }
     
    
     
    $(document).ready(function() {

        $('.card').on('click', function(e) {

            if($(this).hasClass('mt-30')) {
                $(this).removeClass('mt-30');
            } else {
                $(this).addClass('mt-30');
            }
        });

    });
    
    
    $(document).ready(function() {

        $('#variantSelect').on('change', function(e) {

            $('#betButton').attr("disabled", false);
            $('#betButton').css('color', 'white');
            $('#betButton').css('backgroundColor', 'red');

        });
    });

});



















