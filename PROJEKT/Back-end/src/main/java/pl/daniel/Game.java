package pl.daniel;

import pl.daniel.players.Dealer;
import pl.daniel.players.Player;

public class Game {

    private Dealer dealer = new Dealer();
    private Player player = new Player();

    public Game() {
        giveFiveCardsToPlayer();
    }

    private void giveFiveCardsToPlayer() {
        dealer.giveFiveCardsTo(player);
        player.setVariant();
    }

    public String[] getHand(){
        return player.getAllIcons();
    }

    public String getVariant(){
        return player.showVariant();
    }

    public int getRank(){
        return player.getRank();
    }

    public Dealer getDealer() {
        return dealer;
    }

    public Player getPlayer() {
        return player;
    }

    public void replaceCards(boolean[] toChange) {
        for (int index = 0; index < 5; index++) {
            if (toChange[index] == true) {
                dealer.addCard(player.getCardFromHand(index));
                dealer.giveCardTo(player, index);
            }
        }
    }

}

