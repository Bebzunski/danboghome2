package pl.daniel.repository;

import org.springframework.data.repository.CrudRepository;
import pl.daniel.entity.Round;

public interface RoundRepository extends CrudRepository<Round,Long> {
}
