package pl.daniel.repository;

import org.springframework.data.repository.CrudRepository;
import pl.daniel.entity.User;

public interface UserRepository extends CrudRepository<User,Long>{
}
