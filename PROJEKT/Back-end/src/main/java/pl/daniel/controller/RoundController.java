package pl.daniel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.daniel.Game;
import pl.daniel.entity.Round;
import pl.daniel.repository.RoundRepository;

@RestController
@CrossOrigin
@RequestMapping("/game")
public class RoundController {

    private Game game;
    @Autowired
    private RoundRepository roundRepository;

    @RequestMapping("/hand")
    public String[] hand() {
        game = new Game();
        return game.getPlayer().getAllIcons();
    }

    @RequestMapping("/show")
    public String[] showHand() {
        return game.getPlayer().getAllIcons();
    }

    @RequestMapping("/variant")
    public String[] variant() {
        String[] variant = new String[1];
        variant[0] = game.getVariant();
        return variant;
    }

    @RequestMapping("/change")
    public String[] changeCards(@RequestParam(name = "change") String change) {
        String[] split = change.split(",");
        boolean[] toChange = new boolean[5];
        for(int i = 0 ; i < 5 ; i++){
            toChange[i] = Boolean.parseBoolean(split[i]);
        }
        game.replaceCards(toChange);
        return game.getPlayer().getAllIcons();
    }

    @RequestMapping("/rank")
    public String[] showRank(){
        String[] rank = new String[1];
        rank[0] = ""+game.getRank();
        return rank;
    }

    @RequestMapping("/end")
    public Round add(){
        Round round = new Round();
        round.setVariant(game.getVariant());
        round.setRank(game.getRank());
        return roundRepository.save(round);
    }
}
