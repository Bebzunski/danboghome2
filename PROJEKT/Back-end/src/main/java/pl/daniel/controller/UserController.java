package pl.daniel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.daniel.entity.User;
import pl.daniel.repository.UserRepository;

import java.security.MessageDigest;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/add")
    public User add(@RequestParam(name = "name") String name,
                    @RequestParam(name = "password") String password){

        if(!containName(name)) {
            User user = new User();
            user.setName(name);
            user.setPassword(encode(password));
            user.setChips(1000);
            return userRepository.save(user);
        } else {
            return new User();
        }
    }

    @RequestMapping("/show")
    public List<User> showAll() {
        return (List<User>) userRepository.findAll();
    }

    @RequestMapping("/login")
    public String[] contain(@RequestParam(name = "name") String name,
                           @RequestParam(name = "password") String password){
        List<User> users = showAll();
        for(User user : users){
            if(user.getName().equals(name) && user.getPassword().equals(encode(password))){
                return new String[]{""+true};
            }
        }
        return new String[]{""+false};
    }

    public boolean containName(String name){
        List<User> users = showAll();
        for(User user : users){
            if(user.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    public static String encode(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
