package pl.daniel.variants;

import pl.daniel.cards.Figure;

public class TwoPair  extends Variant{

    private Figure highPair;
    private Figure lowPair;

    private Figure pair;
    private final int rank = 600;

    @Override
    public int getRank() {
        return rank;
    }

    public TwoPair(int[] handID) {
        int index = findIndex(handID, 2, 1);
        highPair = figures.getFigure(index);

        index = findIndex(handID, 2, 2);
        lowPair = figures.getFigure(index);
    }

    @Override
    public String showVariant() {
        return "DWIE PARY | "+highPair.getCharFigure()+" na "+lowPair.getCharFigure();
    }

}
