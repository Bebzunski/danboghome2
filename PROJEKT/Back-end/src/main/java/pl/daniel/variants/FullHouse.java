package pl.daniel.variants;


import pl.daniel.cards.Figure;

public class FullHouse  extends Variant {

    private Figure trips;
    private Figure pair;

    private final int rank = 900;

    @Override
    public int getRank() {
        return rank;
    }


    public FullHouse(int[] handID) {

        int index = findIndex(handID, 3, 1);
        trips = figures.getFigure(index);

        index = findIndex(handID, 2, 1);
        pair = figures.getFigure(index);
    }

    @Override
    public String showVariant() {
        return "FULL HOUSE | "+trips.getCharFigure()+" na "+pair.getCharFigure();
    }

}
