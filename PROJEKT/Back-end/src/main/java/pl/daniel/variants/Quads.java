package pl.daniel.variants;

import pl.daniel.cards.Figure;

public class Quads  extends Variant {

    private Figure four;

    private final int rank = 1000;

    @Override
    public int getRank() {
        return rank;
    }

    public Quads(int[] handID) {

        int index = findIndex(handID, 4, 1);
        four = figures.getFigure(index);

    }

    @Override
    public String showVariant() {
        return "KARETA | "+four.getCharFigure();
    }

}

