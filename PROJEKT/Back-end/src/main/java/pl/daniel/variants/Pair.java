package pl.daniel.variants;


import pl.daniel.cards.Figure;

public class Pair extends Variant{

    private Figure pair;
    private final int rank = 500;

    @Override
    public int getRank() {
        return rank;
    }

    public Pair(int[] handID) {

        int index = findIndex(handID, 2, 1);
        pair = figures.getFigure(index);

    }

    @Override
    public String showVariant() {
        return "JEDNA PARA | "+ pair.getCharFigure();
    }

}
