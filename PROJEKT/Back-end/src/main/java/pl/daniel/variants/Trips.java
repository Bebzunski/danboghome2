package pl.daniel.variants;

import pl.daniel.cards.Figure;

public class Trips  extends Variant{

    private Figure trips;

    private final int rank = 700;

    @Override
    public int getRank() {
        return rank;
    }

    public Trips(int[] handID) {

        int index = findIndex(handID, 3, 1);
        trips = figures.getFigure(index);
    }

    @Override
    public String showVariant() {
        return "TRÓJKA | "+trips.getCharFigure();
    }

}
