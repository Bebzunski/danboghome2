package pl.daniel.variants;

import pl.daniel.cards.Figure;

public class HighCard  extends Variant {

    private Figure high1;
    private final int rank = 400;
    @Override
    public int getRank() {
        return rank;
    }

    public HighCard(int[] handID) {
        int index = findIndex(handID, 1, 1);
        high1 = figures.getFigure(index);

    }

    @Override
    public String showVariant() {
        return "WYSOKA KARTA | "+high1.getCharFigure();
    }

}

