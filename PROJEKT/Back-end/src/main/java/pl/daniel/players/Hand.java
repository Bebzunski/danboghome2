package pl.daniel.players;

import pl.daniel.cards.Card;
import pl.daniel.variants.*;

public class Hand {

    private Card[] hand = new Card[5];
    private Variant variant;

    public void setVariant() {

        int[] handID = HandClassifier.getHandId(this);
        int variantID = HandClassifier.getVariantID(this);

        if (variantID == 108 && checkIfStraight(handID)) {
            variant = new Straight(handID);
        } else if(variantID == 108){
            variant = new HighCard(handID);
        } else if (variantID == 209) {
            variant = new Pair(handID);
        } else if (variantID == 210) {
            variant = new TwoPair(handID);
        } else if (variantID == 310) {
            variant = new Trips(handID);
        } else if (variantID == 311) {
            variant = new FullHouse(handID);
        } else if (variantID == 412) {
            variant = new Quads(handID);
        }
    }

    public boolean checkIfStraight(int[] handID){
        int start = Variant.findIndex(handID, 1 , 1);
        int stop = Variant.findIndex(handID, 1 , 5);
        int lenght = stop-start;
        return lenght==4;
    }

    public String showVariant() {
        setVariant();
        return variant.showVariant();
    }


    public Card[] getFiveCardsFromHand() {
        return hand;
    }

    public String getIcon(int index){
        return getFiveCardsFromHand()[index].getIcon();
    }

    public String[] getAllIcons(){
        String[] icons = new String[5];
        for(int i = 0 ; i < 5 ; i++){
            icons[i] = getIcon(i);
        }
        return icons;
    }

    public int length(){
        return hand.length;
    }

    public int getRank(){
        setVariant();
        return variant.getRank();
    }
}

