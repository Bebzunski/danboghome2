package pl.daniel.players;

import pl.daniel.cards.Card;

public class Player {

    private Hand hand = new Hand();
    public boolean[] toChange = new boolean[5];

    public Card getCardFromHand(int index) {
        return hand.getFiveCardsFromHand()[index];
    }

    public Card[] getAllCards() {
        return hand.getFiveCardsFromHand();
    }

    public void setVariant() {
        hand.setVariant();
    }

    public String showVariant() {
        return hand.showVariant();
    }

    public int getRank(){
        return hand.getRank();
    }

    public void replaceCard(Dealer dealer, int index) {
        dealer.addCard(getCardFromHand(index));
        dealer.giveCardTo(this, index);
    }

    public void replaceCards(Dealer dealer) {
        for (int index = 0; index < 5; index++) {
            if (toChange[index] == true) {
                dealer.addCard(getCardFromHand(index));
                dealer.giveCardTo(this, index);
            }
        }
    }

    public String[] getAllIcons(){
        return hand.getAllIcons();
    }

    public String getIcon(int index) {
        return hand.getIcon(index);
    }

}
