package pl.daniel.cards;

public enum Card {

    HA(Suit.HEART, Figure.ACE, "HA" ),
    DA(Suit.DIAMOND, Figure.ACE, "DA"),
    SA(Suit.SPADES, Figure.ACE, "SA"),
    CA(Suit.CLUB, Figure.ACE, "CA"),
    HK(Suit.HEART, Figure.KING, "HK"),
    DK(Suit.DIAMOND, Figure.KING, "DK"),
    SK(Suit.SPADES, Figure.KING, "SK"),
    CK(Suit.CLUB, Figure.KING, "CK"),
    HQ(Suit.HEART, Figure.QUEEN, "HQ"),
    DQ(Suit.DIAMOND, Figure.QUEEN, "DQ"),
    SQ(Suit.SPADES, Figure.QUEEN, "SQ"),
    CQ(Suit.CLUB, Figure.QUEEN, "CQ"),
    HJ(Suit.HEART, Figure.JACK, "HJ"),
    DJ(Suit.DIAMOND, Figure.JACK, "DJ"),
    SJ(Suit.SPADES, Figure.JACK, "SJ"),
    CJ(Suit.CLUB, Figure.JACK, "CJ"),
    HT(Suit.HEART, Figure.TEN, "HT"),
    DT(Suit.DIAMOND, Figure.TEN, "DT"),
    ST(Suit.SPADES, Figure.TEN, "ST"),
    CT(Suit.CLUB, Figure.TEN, "CT"),
    H9(Suit.HEART, Figure.NINE, "H9"),
    D9(Suit.DIAMOND, Figure.NINE, "D9"),
    S9(Suit.SPADES, Figure.NINE, "S9"),
    C9(Suit.CLUB, Figure.NINE, "C9"),
    H8(Suit.HEART, Figure.EIGHT, "H8"),
    D8(Suit.DIAMOND, Figure.EIGHT, "D8"),
    S8(Suit.SPADES, Figure.EIGHT, "S8"),
    C8(Suit.CLUB, Figure.EIGHT, "C8"),
    H7(Suit.HEART, Figure.SEVEN, "H7"),
    D7(Suit.DIAMOND, Figure.SEVEN, "D7"),
    S7(Suit.SPADES, Figure.SEVEN, "S7"),
    C7(Suit.CLUB, Figure.SEVEN, "C7"),
    H6(Suit.HEART, Figure.SIX, "H6"),
    D6(Suit.DIAMOND, Figure.SIX, "D6"),
    S6(Suit.SPADES, Figure.SIX, "S6"),
    C6(Suit.CLUB, Figure.SIX, "C6"),
    H5(Suit.HEART, Figure.FIVE, "H5"),
    D5(Suit.DIAMOND, Figure.FIVE, "D5"),
    S5(Suit.SPADES, Figure.FIVE, "S5"),
    C5(Suit.CLUB, Figure.FIVE, "C5"),
    H4(Suit.HEART, Figure.FOUR, "H4"),
    D4(Suit.DIAMOND, Figure.FOUR, "D4"),
    S4(Suit.SPADES, Figure.FOUR, "S4"),
    C4(Suit.CLUB, Figure.FOUR, "C4"),
    H3(Suit.HEART, Figure.THREE, "H3"),
    D3(Suit.DIAMOND, Figure.THREE, "D3"),
    S3(Suit.SPADES, Figure.THREE, "S3"),
    C3(Suit.CLUB, Figure.THREE, "C3"),
    H2(Suit.HEART, Figure.TWO, "H2"),
    D2(Suit.DIAMOND, Figure.TWO, "D2"),
    S2(Suit.SPADES, Figure.TWO, "S2"),
    C2(Suit.CLUB, Figure.TWO, "C2");

    private Figure figure;
    private Suit suit;
    private String icon;

    private Card(Suit suit, Figure figure , String icon ) {
        this.figure = figure;
        this.suit = suit;
        this.icon = icon;
    }

    public int getRank() {
        return figure.getRank();
    }

    public Suit getSuit() {
        return suit;
    }

    public String getCard(){
        return figure.getCharFigure()+" "+suit.getColour();
    }

    public String getIcon(){
        return icon;
    }

}
