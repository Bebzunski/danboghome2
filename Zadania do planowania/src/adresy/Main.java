package adresy;

public class Main {

	public static void main(String[] args) {

		printAllAddresses("Kopernika", 3);
	}

	public static void printAllAddresses(String street, int lastNmber) {
		for (int staircase = 1; staircase <= lastNmber; staircase += 2) {
			printAllAdressesInBulding(street, staircase);
		}
	}

	private static void printAllAdressesInBulding(String street, int staircase) {
		for (int doorNr = 1; doorNr <= 12; doorNr++) {
			if (doorNr <= 6) {
				printAddress(street, staircase,"A", doorNr);
			} else {
				printAddress(street, staircase,"B", doorNr);
			}
		}
	}

	private static void printAddress(String street, int building, String staircase, int doorNr) {
		System.out.println("ulica: "+street + " " + building + staircase + "/" + doorNr);
	}
}
