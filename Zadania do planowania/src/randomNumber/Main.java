package randomNumber;

import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Main m = new Main();
		m.guessIntNumberFromZeroToHundred();
	}

	private void guessIntNumberFromZeroToHundred() {
		Random r = new Random();
		int randomNumber = r.nextInt(11);
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj liczb� ca�kowit� od 0 do 100.\nMasz 10 pr�b.");
		int currentNumber;
		for (int i = 1; i <= 10; i++) {
			currentNumber = sc.nextInt();
			if (currentNumber == randomNumber) {
				System.out.println("Gratuluj� !! Trafi�e� !!");
				break;
			} else if (i < 10) {
				System.out.println("Pon�w pr�b�.");
			} else {
				System.out.println("Niestety nie uda�o si� trafi� wylosowanej liczby.");
			}
		}
		sc.close();
	}

}
