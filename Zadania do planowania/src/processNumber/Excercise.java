package processNumber;

public class Excercise {

	public int[] processNumber(int a, int b) throws IllegalArgumentException {

		if ((!isInRange(a, 0, 255)) || (!isInRange(b, a, 255))) {

			throw new IllegalArgumentException("IllegalArgumentException");
		}

		int numberOfElement = b - a + 1;
		int[] ret = new int[numberOfElement];

		int firtsOdd = a % 2; // if 1, then 'a' is odd number
		int lastEven = (b + 1) % 2; // if 1, then 'b' is even number

		int numberOfOdd = numberOfElement / 2 + zeroOrOneFromConjunction(firtsOdd == 1, lastEven == 0);

		int numberOfEven = numberOfElement - numberOfOdd;

		putElementsOfSequence(ret, 0, a + firtsOdd, 2, numberOfEven);

		putElementsOfSequence(ret, numberOfEven, b - lastEven, -2, numberOfOdd);

		return ret;

	}

	public boolean isInRange(int arg, int left, int right) {
		return (arg >= left && arg <= right);
	}

	public void putElementsOfSequence(int[] array, int startingIndex, int startingValue, int difference, int n) {
		for (int i = startingIndex; i < startingIndex + n; i++) {
			array[i] = startingValue + difference * (i - startingIndex);
		}
	}

	public int zeroOrOneFromConjunction(boolean b1, boolean b2) {
		int ret = 0;
		if (b1 == true && b2 == true) {
			ret = 1;
		}
		return ret;
	}
}
