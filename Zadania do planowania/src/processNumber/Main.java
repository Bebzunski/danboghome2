package processNumber;


public class Main {

	public static void main(String[] args) {
		
		Excercise ex = new Excercise();
		
		for(int num : ex.processNumber(2, 6)){
			System.out.print(num+" ");
		}
		
		System.out.println();
		for(int num : ex.processNumber(2, 7)){
			System.out.print(num+" ");
		}
		
		System.out.println();
		for(int num : ex.processNumber(1, 6)){
			System.out.print(num+" ");
		}
		
		System.out.println();
		for(int num : ex.processNumber(1, 5)){
			System.out.print(num+" ");
		}
		
	}
	
}
